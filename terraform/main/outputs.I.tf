output "I-ci" {
  value = {
    name              = aws_iam_user.ci.name
    path              = aws_iam_user.ci.path
    arn               = aws_iam_user.ci.arn
    aws_access_key_id = aws_iam_access_key.ci.id
  }
}

output "I-ui_ci-aws_secret_access_key" {
  value     = aws_iam_access_key.ci.secret
  sensitive = true
}

output "I-auth0" {
  value = {
    name              = aws_iam_user.auth0.name
    path              = aws_iam_user.auth0.path
    arn               = aws_iam_user.auth0.arn
    aws_access_key_id = aws_iam_access_key.auth0.id
  }
}

output "I-ui_auth0-aws_secret_access_key" {
  value     = aws_iam_access_key.auth0.secret
  sensitive = true
}

output "I-automated_test" {
  value = {
    name = aws_iam_role.automated_test.name
    path = aws_iam_role.automated_test.path
    arn  = aws_iam_role.automated_test.arn
  }
}

output "I-custom_domain-dev" {
  value = module.custom_domain-development.I-fqdn
}

output "I-custom_domain-prod" {
  value = "NIY" // module.custom_domain-production.I-fqdn
}

output "X-verification-dev" {
  value = module.custom_domain-development.X-verification
}
