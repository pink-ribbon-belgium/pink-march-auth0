data "aws_iam_policy_document" "automated_test" {
  statement {
    # can test with real DynamoDB Table
    effect  = "Allow"
    actions = module.actions.I-dynamodb-items-readwrite
    resources = [
      data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-test["arn"],
      "${data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-test["arn"]}/index/*",
      data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-production["arn"],
      "${data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-production["arn"]}/index/*"
    ]
  }
}

resource "aws_iam_policy" "automated_test" {
  name        = data.terraform_remote_state.infrastructure_role.outputs.II-automated_test["name"]
  path        = data.terraform_remote_state.infrastructure_role.outputs.II-automated_test["path"]
  policy      = data.aws_iam_policy_document.automated_test.json
  description = "Allow to test with DynamoDB table"
}
