resource "aws_iam_user" "auth0" {
  name = data.terraform_remote_state.infrastructure_role.outputs.II-auth0["name"]
  path = data.terraform_remote_state.infrastructure_role.outputs.II-auth0["path"]
  tags = local.tags
}

resource "aws_iam_access_key" "auth0" {
  user = aws_iam_user.auth0.name
}

resource "aws_iam_user_policy_attachment" "auth0" {
  user       = aws_iam_user.auth0.name
  policy_arn = aws_iam_policy.auth0.arn
}

