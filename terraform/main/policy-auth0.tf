data "aws_iam_policy_document" "auth0" {
  statement {
    # can test with real DynamoDB Table
    effect  = "Allow"
    actions = module.actions.I-dynamodb-items-readwrite
    resources = [
      data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-test["arn"],
      "${data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-test["arn"]}/index/*",
      data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-production["arn"],
      "${data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-production["arn"]}/index/*"
    ]
  }

  statement {
    effect  = "Allow"
    actions = module.actions.I-iam-read
    resources = [
      data.terraform_remote_state.infrastructure_role.outputs.II-auth0["arn-user"]
    ]
  }
}

resource "aws_iam_policy" "auth0" {
  name        = data.terraform_remote_state.infrastructure_role.outputs.II-auth0["name"]
  path        = data.terraform_remote_state.infrastructure_role.outputs.II-auth0["path"]
  policy      = data.aws_iam_policy_document.auth0.json
  description = "Allow to work with items in DynamoDB table"
}
