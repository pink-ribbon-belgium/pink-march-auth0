variable "auth0-domain-dev" {
  description = "Domain of the client `auth0-text-management` in the Auth0 development tenant"
  type        = string
}

variable "auth0-client_id-dev" {
  description = "Client ID of the client `auth0-text-management` in the Auth0 development tenant"
  type        = string
}

variable "auth0-client_secret-dev" {
  description = "Client secret of the client `auth0-text-management` in the Auth0 development tenant"
  type        = string
}

variable "auth0-domain-prod" {
  description = "Domain of the client `auth0-text-management` in the Auth0 production tenant"
  type        = string
}

variable "auth0-client_id-prod" {
  description = "Client ID of the client `auth0-text-management` in the Auth0 production tenant"
  type        = string
}

variable "auth0-client_secret-prod" {
  description = "Client secret of the client `auth0-text-management` in the Auth0 production tenant"
  type        = string
}
