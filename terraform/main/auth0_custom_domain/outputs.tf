output "I-fqdn" {
  value = auth0_custom_domain.main.domain
}

output "X-verification" {
  value = auth0_custom_domain.main.verification
}

