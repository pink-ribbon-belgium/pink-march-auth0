locals {
  fqdn-base            = "${var.fqdn-prefix}.${var.hosted_zone["name"]}"
  fqdn                 = trimsuffix(local.fqdn-base, ".")
  ttl                  = 900
  verification_methods = auth0_custom_domain.main.verification[0].methods
  cname                = [for m in local.verification_methods : lookup(m, "record") if lookup(m, "name") == "cname"][0]
}
