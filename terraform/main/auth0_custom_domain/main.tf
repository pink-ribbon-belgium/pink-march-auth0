resource "auth0_custom_domain" "main" {
  domain              = local.fqdn
  type                = "auth0_managed_certs"
  verification_method = "txt"
}
