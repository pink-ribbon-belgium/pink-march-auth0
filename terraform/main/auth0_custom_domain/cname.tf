resource "aws_route53_record" "custom_domain" {
  zone_id = var.hosted_zone["id"]
  name    = local.fqdn
  type    = "CNAME"
  records = [local.cname]
  ttl     = local.ttl
}
