provider "auth0" {
  domain        = var.auth0-domain
  client_id     = var.auth0-client_id
  client_secret = var.auth0-client_secret
  version       = "~> 0.13"
}
