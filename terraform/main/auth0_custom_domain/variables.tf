variable "auth0-domain" {
  description = "Domain of the client `auth0-text-management` in the Auth0 tenant"
  type        = string
}

variable "auth0-client_id" {
  description = "Client ID of the client `auth0-text-management` in the Auth0 tenant"
  type        = string
}

variable "auth0-client_secret" {
  description = "Client secret of the client `auth0-text-management` in the Auth0 tenant"
  type        = string
}

variable "hosted_zone" {
  description = "Map of the hosted zone to create the DNS entries for the custom domain in. Contains the `id` and the `name`"
  type        = map(string)
}

variable "fqdn-prefix" {
  description = "prefix of the fqdn of the Auth0 custom domain"
  type        = string
}
