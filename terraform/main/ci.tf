resource "aws_iam_user" "ci" {
  name = data.terraform_remote_state.infrastructure_role.outputs.II-ci["name"]
  path = data.terraform_remote_state.infrastructure_role.outputs.II-ci["path"]
  tags = local.tags
}

resource "aws_iam_access_key" "ci" {
  user = aws_iam_user.ci.name
}

resource "aws_iam_user_policy_attachment" "ci-automated_test" {
  user       = aws_iam_user.ci.name
  policy_arn = aws_iam_policy.automated_test.arn
}

