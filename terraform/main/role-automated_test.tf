data "aws_iam_policy_document" "automated-test-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
    condition {
      test = "StringEquals"
      values = [
        "devsecops"
      ]
      variable = "aws:PrincipalTag/canAssumeRole"
    }
  }
}

resource "aws_iam_role" "automated_test" {
  name               = data.terraform_remote_state.infrastructure_role.outputs.II-automated_test["name"]
  path               = data.terraform_remote_state.infrastructure_role.outputs.II-automated_test["path"]
  assume_role_policy = data.aws_iam_policy_document.automated-test-assume-role-policy.json
  tags               = local.tags
}

resource "aws_iam_role_policy_attachment" "automated_test" {
  policy_arn = aws_iam_policy.automated_test.arn
  role       = aws_iam_role.automated_test.name
}
