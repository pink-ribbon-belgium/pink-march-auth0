# pink-march-auth0 terraform main

## Credentials

This configuration defines resources in AWS _and Auth0_, for both the development and production tenant.

Therefore, apart from the credentials for AWS, which are acquired in the traditional way (via a profile), we also need
credentials to manage Auth0.

The credentials to manage Auth0 are the credentials of the
[`auth0-text-management` client](../../clients/auth0-text-management.json) defined elsewhere in this repository. It has,
amongst others, the [grants to manage custom domains](../../grants/auth0-text-management.json). Note that there us a
separate client for each Auth0 tenant (development and production).

Terraform gets the credentials for these clients from [variables](./variables.tf).

You can supply the credentials to Terraform in
[different ways](https://www.terraform.io/docs/configuration/variables.html), but the easiest is to add a file
[`auth0-credentials.auto.tfvars`](auth0-credentials.auto.tfvars) to this directory (it is git-ignored) with the
credentials. It will be automatically loaded.

To get the credentials

- log in to the Auth0 console for the tenant in question
- go to **Applications** / **auth0-text-management** / **Settings**
- copy the credentials into [`auth0-credentials.auto.tfvars`](auth0-credentials.auto.tfvars)


    auth0-domain-dev        = "<AUTH0 DEVELOPMENT auth0-text-management DOMAIN>"
    auth0-client_id-dev     = "<AUTH0 DEVELOPMENT auth0-text-management CLIENT_ID>"
    auth0-client_secret-dev = "<AUTH0 DEVELOPMENT auth0-text-management CLIENT_SECRET>"

    auth0-domain-prod        = "<AUTH0 PRODUCTION auth0-text-management DOMAIN>"
    auth0-client_id-prod     = "<AUTH0 PRODUCTION auth0-text-management CLIENT_ID>"
    auth0-client_secret-prod = "<AUTH0 PRODUCTION auth0-text-management CLIENT_SECRET>"

## Application

Once applied, it seems that you _still_ need to click **VERIFY** in the Auth0 console (**Settings** / **Custom
Domains**).

After that, it takes some minutes before everything is setup. Click **Settings** / **Custom Domains** / **TEST** to
test.

## Weirdness

The [documentation](https://auth0.com/docs/custom-domains/configure-custom-domains-with-auth0-managed-certificates) says
to add the `CNAME` to DNS (_Verify ownership_ / _1._ and _2._). Then it talks about the `CNAME` record again (_Add CNAME
verification record to DNS record_), and about a `TXT` record. We don't seem to need that here? It also does not make
sense, because with the `CNAME` we _did_ prove ownership of the domain.

## CAA

We have set up `CAA` records for all domain names that point to the `app` Cloudfront, because we use TLS there, to only
allow certificates from the AWS certificate authority:

- `app.pink-march.pink-ribbon-belgium.org`
- `demo.pink-march.pink-ribbon-belgium.org`
- `june2020.pink-march.pink-ribbon-belgium.org`
- `october2020.pink-march.pink-ribbon-belgium.org`


    >dig CAA app.pink-march.pink-ribbon-belgium.org

    ; <<>> DiG 9.10.6 <<>> CAA app.pink-march.pink-ribbon-belgium.org
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 35948
    ;; flags: qr rd ra; QUERY: 1, ANSWER: 10, AUTHORITY: 13, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 4096
    ;; QUESTION SECTION:
    ;app.pink-march.pink-ribbon-belgium.org.        IN CAA

    ;; ANSWER SECTION:
    app.pink-march.pink-ribbon-belgium.org. 300 IN CAA 0 issuewild "awstrust.com"
    app.pink-march.pink-ribbon-belgium.org. 300 IN CAA 0 issuewild "amazontrust.com"
    app.pink-march.pink-ribbon-belgium.org. 300 IN CAA 0 issue "amazonaws.com"
    app.pink-march.pink-ribbon-belgium.org. 300 IN CAA 0 issue "awstrust.com"
    app.pink-march.pink-ribbon-belgium.org. 300 IN CAA 0 issue "amazontrust.com"
    app.pink-march.pink-ribbon-belgium.org. 300 IN CAA 0 issuewild "amazon.com"
    app.pink-march.pink-ribbon-belgium.org. 300 IN CAA 0 issuewild "amazonaws.com"
    app.pink-march.pink-ribbon-belgium.org. 300 IN CAA 0 issue ";"
    app.pink-march.pink-ribbon-belgium.org. 300 IN CAA 0 issue "amazon.com"
    app.pink-march.pink-ribbon-belgium.org. 300 IN CAA 0 issuewild ";"

Obviously, the Auth0 custom domains uses TLS too. And Auth0 manages those certificates. We do not know which certificate
authority is used by Auth0 (wild guess though: Let's Encrypt?).

There are currently no `CAA` records set up for this, but we now could inspect such a certificate, and add `CAA` records
for the custom domain.
