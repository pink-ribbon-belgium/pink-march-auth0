module "custom_domain-development" {
  source              = "./auth0_custom_domain"
  auth0-domain        = var.auth0-domain-dev
  auth0-client_id     = var.auth0-client_id-dev
  auth0-client_secret = var.auth0-client_secret-dev
  hosted_zone         = data.terraform_remote_state.dns.outputs.I-hosted_zone-pink_ribbon_belgium_org
  fqdn-prefix         = local.custom_domain-prefix-dev
}

module "custom_domain-production" {
  source              = "./auth0_custom_domain"
  auth0-domain        = var.auth0-domain-prod
  auth0-client_id     = var.auth0-client_id-prod
  auth0-client_secret = var.auth0-client_secret-prod
  hosted_zone         = data.terraform_remote_state.dns.outputs.I-hosted_zone-pink_ribbon_belgium_org
  fqdn-prefix         = local.custom_domain-prefix-prod
}
