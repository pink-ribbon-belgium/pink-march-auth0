locals {
  region                    = "eu-west-1"
  profile                   = "pink-ribbon-belgium-dev"
  repo                      = "pink-march-auth0"
  custom_domain-prefix-dev  = "auth-dev.pink-march"
  custom_domain-prefix-prod = "auth.pink-march"
  tags = {
    name = "Pink March Auth0"
    env  = "production"
    repo = "${local.repo}/terraform/main"
  }

}

data "aws_caller_identity" "current" {}

module "actions" {
  source = "github.com/peopleware/terraform-ppwcode-modules//actions?ref=v%2F6%2F0%2F0"
}
