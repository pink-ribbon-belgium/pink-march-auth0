output "I-infrastructure_role" {
  value = {
    path = aws_iam_role.pink-march-auth0-infrastructure.path
    name = aws_iam_role.pink-march-auth0-infrastructure.name
    arn  = aws_iam_role.pink-march-auth0-infrastructure.arn
  }
}

output "I-ci" {
  value = {
    name       = local.ci-name
    path       = local.ci-path
    arn-user   = local.ci-arn-user
    arn-policy = local.ci-arn-policy
  }
}

output "I-auth0" {
  value = {
    name       = local.auth0-name
    path       = local.auth0-path
    arn-user   = local.auth0-arn-user
    arn-policy = local.auth0-arn-policy
  }
}

output "I-automated_test" {
  value = {
    name       = local.automated_test-name
    path       = local.automated_test-path
    arn-role   = local.automated_test-arn-role
    arn-policy = local.automated_test-arn-policy
  }
}

output "I-dynamodb_table" {
  value = {
    name = "no longer supported"
    arn  = "no longer supported"
  }
}

