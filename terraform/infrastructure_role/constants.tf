locals {
  region                    = "eu-west-1"
  profile                   = "pink-ribbon-belgium-dev"
  repo-basename             = "pink-march-auth0"
  infrastructure_role-name  = "${local.repo-basename}-infrastructure"
  ci-name                   = "ci-${local.repo-basename}"
  ci-path                   = "/ci/"
  ci-arn-user               = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user${local.ci-path}${local.ci-name}"
  ci-arn-policy             = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy${local.ci-path}${local.ci-name}"
  auth0-name                = local.repo-basename
  auth0-path                = "/auth0/"
  auth0-arn-user            = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user${local.auth0-path}${local.auth0-name}"
  auth0-arn-policy          = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy${local.auth0-path}${local.auth0-name}"
  automated_test-name       = "automated-test-${local.repo-basename}"
  automated_test-path       = "/automated-test/"
  automated_test-arn-role   = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role${local.automated_test-path}${local.automated_test-name}"
  automated_test-arn-policy = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy${local.automated_test-path}${local.automated_test-name}"
  # TODO: import from pink-march-service code
  dynamodb_table-name-test       = "pink-march-service-test"
  dynamodb_table-arn-test        = "arn:aws:dynamodb:${local.region}:${data.aws_caller_identity.current.account_id}:table/${local.dynamodb_table-name-test}"
  dynamodb_table-name-production = "pink-march-service-production"
  dynamodb_table-arn-production  = "arn:aws:dynamodb:${local.region}:${data.aws_caller_identity.current.account_id}:table/${local.dynamodb_table-name-production}"
}

data "aws_caller_identity" "current" {}

module "actions" {
  source = "github.com/peopleware/terraform-ppwcode-modules//actions?ref=v%2F6%2F0%2F0"
}
