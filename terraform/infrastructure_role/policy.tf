data "aws_iam_policy_document" "role-policy" {
  statement {
    # manage dynamodb table
    effect = "Allow"
    actions = concat(
      module.actions.I-dynamodb-tables-describe,
      module.actions.I-dynamodb-table-define,
      [
        // TODO These permissions seem far to wide. Check and limit.
        "dynamodb:DescribeTable",
        "dynamodb:DescribeContinuousBackups",
        "dynamodb:DescribeContributorInsights",
        "dynamodb:DescribeGlobalTable",
        "dynamodb:DescribeGlobalTableSettings",
        "dynamodb:DescribeTableReplicaAutoScaling",
        "dynamodb:DescribeTimeToLive",
        "dynamodb:ListTagsOfResource",
        "dynamodb:TagResource",
        "dynamodb:UntagResource",
        "dynamodb:CreateTableReplica",
        "dynamodb:DeleteTableReplica",
        "dynamodb:CreateGlobalTable",
        "dynamodb:UpdateGlobalTable",
        "dynamodb:UpdateGlobalTableSettings",
        "dynamodb:CreateBackup",
        "dynamodb:RestoreTableFromBackup",
        "dynamodb:RestoreTableToPointInTime",
        "dynamodb:UpdateContinuousBackups",
        "dynamodb:UpdateContributorInsights",
        "dynamodb:UpdateTableReplicaAutoScaling",
        "dynamodb:UpdateTimeToLive"
      ]
    )
    resources = [
      local.dynamodb_table-arn-test,
      local.dynamodb_table-arn-production
    ]
  }

  statement {
    # manage CI and auth0 user
    effect = "Allow"
    actions = [
      "iam:Get*",
      "iam:List*",
      "iam:CreateUser",
      "iam:UpdateUser",
      "iam:DeleteUser",
      "iam:TagUser",
      "iam:UntagUser",
      "iam:AttachUserPolicy",
      "iam:DetachUserPolicy",
      "iam:CreateAccessKey",
      "iam:UpdateAccessKey",
      "iam:DeleteAccessKey"
    ]
    resources = [
      local.ci-arn-user,
      local.auth0-arn-user
    ]
  }

  statement {
    # manage automated test and auth0 execution policy and user policies
    effect = "Allow"
    actions = [
      "iam:Get*",
      "iam:List*",
      "iam:CreatePolicy",
      "iam:DeletePolicy",
      "iam:GetPolicyVersion",
      "iam:CreatePolicyVersion",
      "iam:DeletePolicyVersion",
      "iam:SetDefaultPolicyVersion",
    ]
    resources = [
      local.automated_test-arn-policy,
      local.ci-arn-policy,
      local.auth0-arn-policy
    ]
  }

  statement {
    # manage automated test and auth0 execution role
    effect = "Allow"
    actions = [
      "iam:AttachRolePolicy",
      "iam:CreateRole",
      "iam:DeleteRole",
      "iam:DeleteRolePermissionsBoundary",
      "iam:DeleteRolePolicy",
      "iam:DetachRolePolicy",
      "iam:GetRole",
      "iam:GetRolePolicy",
      "iam:ListRolePolicies",
      "iam:ListRoleTags",
      "iam:ListAttachedRolePolicies",
      "iam:PutRolePermissionsBoundary",
      "iam:PutRolePolicy",
      "iam:TagRole",
      "iam:UntagRole",
      "iam:UpdateAssumeRolePolicy",
      "iam:UpdateRole",
      "iam:UpdateRoleDescription",
      "iam:UpdateRole"
    ]
    resources = [
      local.automated_test-arn-role
    ]
  }

  statement {
    # manage custom domains
    effect = "Allow"
    actions = [
      "route53:ChangeResourceRecordSets",
      "route53:GetHostedZone",
      "route53:ListResourceRecordSets",
    ]
    resources = [
      "arn:aws:route53:::hostedzone/${data.terraform_remote_state.dns.outputs.I-hosted_zone-pink_ribbon_belgium_org["id"]}"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "route53:GetChange"
    ]
    resources = ["*"]
  }

}

resource "aws_iam_policy" "role-policy" {
  name        = local.infrastructure_role-name
  policy      = data.aws_iam_policy_document.role-policy.json
  path        = "/devsecops/"
  description = "Can manage ${local.repo-basename} CI user and policies"
}
