/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* global UnauthorizedError,configuration */

/**
 * @interface User
 * @property {string} user_id
 * @property {string} sub_hashed
 * @property {string} name
 * @property {string} nickname
 * @property {string} email
 */

/**
 * @interface Request
 * @property {object} [query]
 * @property {object} [body]
 */

/**
 * @interface IdToken
 */

/**
 * @interface Context
 * @property {string} [sessionID]
 * @property {string} clientID
 * @property {string} clientName
 * @property {Request} request
 * @property {IdToken} idToken
 */

/**
 * Get extra information about the user from DynamoDB, and add it to the id token.
 *
 * @param {User} user
 * @param {Context} context
 * @param {function} callback
 */
// eslint-disable-next-line no-unused-vars
function adornAccessTokenWithRAAs (user, context, callback) {
  /* define constants */
  const ruleName = 'adornAccessTokenWithRAAs'
  const accountIdPattern = /[a-zA-Z0-9]{10}/
  const namespace = 'https://pink-ribbon-belgium.org/'
  const raasClaim = `${namespace}raas`
  const modeParameterName = 'mode' // no namespace (difficult in query)
  // keep in sync with pink-march-service//lib/ppwcode/Mode.js
  const uuidPattern = '[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}'
  const regExpString = `^production|automated-test-${uuidPattern}|qa-\\d+|acceptance-\\d+|demo|june2020|dev-experiment$`
  const modePattern = new RegExp(regExpString)
  const AWSRegion = 'eu-west-1'
  const AWSAccessKeyIdEnvName = 'AWSaccessKeyId'
  const AWSSecretAccessKeyEnvName = 'AWSsecretAccessKey'
  /* Session token is for use in local automated tests, when assuming automated-test role.
     Do not provide this in production or on CI. */
  const AWSSessionTokenEnvName = 'AWSSessionToken'
  const tableNameTest = 'pink-march-service-test'
  const tableNameProduction = 'pink-march-service-production'
  const productionModesPattern = /^(production|demo|june2020)$/
  const IndexName = 'Index_A'
  const partitionKeyName = 'partition_key_A'
  const sortKeyName = 'sort_key_A'

  /* setup utilities */
  const assert = require('assert')

  assert(user && typeof user === 'object')
  assert(user.user_id && typeof user.user_id === 'string')
  assert(context && typeof context === 'object')
  assert(!context.sessionID || typeof context.sessionID === 'string')
  assert(context.request && typeof context.request === 'object')
  assert(!context.request.query || typeof context.request.query === 'object')
  assert(!context.request.body || typeof context.request.body === 'object')
  assert(context.request.query || context.request.body) // because we run after ensureAccount
  assert((context.request.query && context.request.query.mode) || context.request.body.mode) // because we run after ensureAccount
  assert(modePattern.test((context.request.query && context.request.query.mode) || context.request.body.mode)) // because we run after ensureAccount
  assert(context.clientID && typeof context.clientID === 'string')
  assert(context.clientName && typeof context.clientName === 'string')
  assert(context.idToken && typeof context.idToken === 'object')
  assert(context.accessToken && typeof context.idToken === 'object')
  assert(callback && typeof callback === 'function')
  // precondition: run after `ensureAccount`
  assert(user.app_metadata && typeof user.app_metadata === 'object')
  assert.strictEqual(user.app_metadata.structureVersion, 1)
  assert(
    user.app_metadata.accountId &&
      typeof user.app_metadata.accountId === 'string' &&
      accountIdPattern.test(user.app_metadata.accountId)
  )

  let logCounter = 0
  let debugCounter = 0

  // setup debug functions
  function message (str) {
    return `${context.sessionID || '--no session id--'}: ${ruleName}: ${str}`
  }

  function debug (str) {
    if (configuration[`${ruleName}-debug`]) {
      console.log(message('DEBUG (' + logCounter + '.' + debugCounter + '): ' + str))
      debugCounter++
    }
  }

  function info (str) {
    console.info(message('(' + logCounter + '): ' + str))
    logCounter++
    debugCounter = 0
  }

  /* istanbul ignore next : only used in the final catch, which cannot be tested because aws-sdk-mock does not work with
     the older version of aws-sdk Auth0 imposes us to work */
  function warn (str) {
    console.warn(message('(' + logCounter + ') WARNING: ' + str))
    logCounter++
    debugCounter = 0
  }

  /**
   * Get a request parameter from the context
   */
  function parameter (paramName) {
    // make sure 0, and the empty string from a query are not considered as it doesn't exist
    return context.request.query && context.request.query[paramName] !== undefined
      ? context.request.query[paramName]
      : context.request.body && context.request.body[paramName]
  }

  /* istanbul ignore next : only used in the final catch, which cannot be tested because aws-sdk-mock does not work with
     the older version of aws-sdk Auth0 imposes us to work */
  function unauthorized (msg) {
    info(msg)
    callback(new UnauthorizedError(message(msg)))
  }

  /* log call */
  info('Node Version: ' + process.version)
  debug('user: ' + JSON.stringify(user))
  debug('context: ' + JSON.stringify(context))
  const userRepresentation = user.user_id + ' (' + user.name + ', ' + user.nickname + ', ' + user.email + ')'
  const clientRepresentation = context.clientName + ' (' + context.clientID + ')'

  /* execute */
  const mode = parameter(modeParameterName)
  const accountId = user.app_metadata.accountId
  const contextMessage = `token request by ${clientRepresentation} for ${userRepresentation} with mode '${mode}' and accountId ${accountId}`
  info(contextMessage)

  // precondition: must run after ensureAccount, which already validated the `mode`
  assert(mode && modePattern.test(mode))

  // fixed version (https://auth0-extensions.github.io/canirequire/#aws-sdk)
  const awsSdkReference =
    process.env.NODE_ENV === 'mocha'
      ? 'aws-sdk'
      : /* istanbul ignore next: cannot cover code specific for Auth0 in tests - it is node-incompatible */ 'aws-sdk@2.593.0'
  const AWS = require(awsSdkReference) // only load when needed
  AWS.config.apiVersions = {
    dynamodb: '2012-08-10'
  }
  AWS.config.region = AWSRegion
  AWS.config.credentials = new AWS.Credentials(
    configuration[AWSAccessKeyIdEnvName],
    configuration[AWSSecretAccessKeyEnvName],
    configuration[AWSSessionTokenEnvName]
  )
  const dynamodb = new AWS.DynamoDB.DocumentClient()

  info(`getting administration for ${accountId} …`)
  const indexAPartitionKey = `/${mode}/account/${accountId}/administratorOf`
  debug(`indexAPartitionKey: ${indexAPartitionKey}`)

  dynamodb
    .query({
      TableName: productionModesPattern.test(mode)
        ? /* istanbul ignore next : no automated test on production table */ tableNameProduction
        : tableNameTest,
      IndexName,
      ExpressionAttributeNames: {
        '#partitionKey': partitionKeyName,
        '#sortKey': sortKeyName
      },
      ExpressionAttributeValues: {
        ':partitionKey': indexAPartitionKey,
        ':sortKey': 'actual'
      },
      KeyConditionExpression: '#partitionKey = :partitionKey and #sortKey = :sortKey',
      // TODO check that we do not take into account deleted things
      ScanIndexForward: false
    })
    .promise()
    .then(result => {
      const administrationGroupIds = result.Items.map(item => item.data.groupId)

      const groupAdministrations = result.Items.map(item => item.data)

      info(`received administration group ids: [${administrationGroupIds.join(', ')}]`)

      // NOTE: no RAAs for vouchers, slots, group/administrators/*: remove from API

      function administratorOfRAAs (groupType, groupId) {
        return [
          `{GET,PUT}:/I/${groupType}/${groupId}`,
          `PUT:/I/${groupType}/${groupId}/publicProfile`,
          `{GET,PUT}:/I/${groupType}/${groupId}/administrators/*`,
          `{GET,PUT}:/I/${groupType}/${groupId}/slot/*`,
          `POST:/I/${groupType}/${groupId}/payment`,
          `GET:/I/${groupType}/${groupId}/payments`,
          `PUT:/I/${groupType}/${groupId}/payment/*/process/${accountId}`,
          `GET:/I/${groupType}/${groupId}/slots`
        ]
      }

      const raas = [
        'GET:/I/health',
        `GET:/I/account/${accountId}`,
        `{GET,PUT}:/I/account/${accountId}/{publicProfile,preferences}`,
        `{GET,PUT}:/I/account/${accountId}/slot`,
        `GET:/I/account/${accountId}/administratorOf`,
        `{GET,PUT,DELETE}:/I/account/${accountId}/trackerConnection`,
        `GET:/I/account/${accountId}/activities`,
        `PUT:/I/account/${accountId}/activity/*`,
        'POST:/I/activity/calculation',
        `{GET,PUT}:/I/accountaggregate/${accountId}`,
        'GET:/I/accountaggregate/group/*',
        'GET:/I/accountaggregate/subgroup/*',
        'GET:/I/{team,company}/*/publicProfile',
        'GET:/I/group/*/members',
        // Create groups
        'POST:/I/team',
        'POST:/I/company',
        'POST:/I/subgroup',
        // Inspect any group
        'GET:/I/team/*/verification',
        'GET:/I/company/*/verification',
        'GET:/I/company/*/subgroups',
        // Claim Slot
        `PUT:/I/team/*/claimSlot/${accountId}`,
        `PUT:/I/company/*/claimSlot/${accountId}`,
        // Get any calculation
        'POST:/I/payment/calculation',
        // Delete members
        'PUT:/I/team/*/slot/*',
        'PUT:/I/company/*/slot/*',
        'PUT:/I/subgroup/*/company/*/slot/*',
        'GET:/I/subgroup/*/company/*',
        'PUT:/I/subgroup/*/company/*',
        'DELETE:/I/subgroup/*/company/*',
        'GET:/I/subgroup/*/company/*/members',
        'GET:/I/teamaggregate/*',
        'GET:/I/teamaggregate/company/*',
        'GET:/I/teamaggregate/all',
        'GET:/I/companyaggregate/*',
        'GET:/I/companyaggregate/unit/*',
        'GET:/I/companyaggregate/all',
        'GET:/I/statistics'
      ].concat(
        groupAdministrations.reduce(
          (acc, administration) =>
            acc.concat(
              /* prettier-ignore */
              administration.groupType
                ? administratorOfRAAs(administration.groupType.toLowerCase(), administration.groupId)
                : administratorOfRAAs('team', administration.groupId).concat(
                  administratorOfRAAs('company', administration.groupId)
                )
            ),
          []
        )
      )

      context.accessToken[raasClaim] = raas

      // log outcome
      info(`${contextMessage} access tokens has claim '${raasClaim}: [${raas.join(', ')}]'`)

      return callback(null, user, context)
    })
    .catch(
      /* istanbul ignore next : only used in the final catch, which cannot be tested because aws-sdk-mock does not work with
         the older version of aws-sdk Auth0 imposes us to work */
      err => {
        warn(`error getting administration from DynamoDB: ${err.message}`)
        return unauthorized(`could not get group administrations from DynamoDB for ${contextMessage}`)
      }
    )
}
