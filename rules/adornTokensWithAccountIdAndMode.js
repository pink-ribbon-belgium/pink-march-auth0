/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* global configuration */

/**
 * @interface User
 * @property {string} user_id
 * @property {string} sub_hashed
 * @property {string} name
 * @property {string} nickname
 * @property {string} email
 */

/**
 * @interface Request
 * @property {object} [query]
 * @property {object} [body]
 */

/**
 * @interface IdToken
 */

/**
 * @interface Context
 * @property {string} [sessionID]
 * @property {string} clientID
 * @property {string} clientName
 * @property {Request} request
 * @property {IdToken} idToken
 */

/**
 * Get extra information about the user from DynamoDB, and add it to the id token.
 *
 * @param {User} user
 * @param {Context} context
 * @param {function} callback
 */
// eslint-disable-next-line no-unused-vars
function adornTokensWithAccountIdAndMode (user, context, callback) {
  /* define constants */
  const ruleName = 'adornTokensWithAccountIdAndMode'
  const accountIdPattern = /[a-zA-Z0-9]{10}/
  const namespace = 'https://pink-ribbon-belgium.org/'
  const accountIdClaim = `${namespace}accountId`
  const modeParameterName = 'mode' // no namespace (difficult in query)
  // keep in sync with pink-march-service//lib/ppwcode/Mode.js
  const uuidPattern = '[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}'
  const regExpString = `^production|automated-test-${uuidPattern}|qa-\\d+|acceptance-\\d+|demo|june2020|dev-experiment$`
  const modePattern = new RegExp(regExpString)
  const modeClaim = `${namespace}mode`

  /* setup utilities */
  const assert = require('assert')

  assert(user && typeof user === 'object')
  assert(user.user_id && typeof user.user_id === 'string')
  assert(context && typeof context === 'object')
  assert(!context.sessionID || typeof context.sessionID === 'string')
  assert(context.request && typeof context.request === 'object')
  assert(!context.request.query || typeof context.request.query === 'object')
  assert(!context.request.body || typeof context.request.body === 'object')
  assert(context.request.query || context.request.body) // because we run after ensureAccount
  assert((context.request.query && context.request.query.mode) || context.request.body.mode) // because we run after ensureAccount
  assert(modePattern.test((context.request.query && context.request.query.mode) || context.request.body.mode)) // because we run after ensureAccount
  assert(context.clientID && typeof context.clientID === 'string')
  assert(context.clientName && typeof context.clientName === 'string')
  assert(context.idToken && typeof context.idToken === 'object')
  assert(context.accessToken && typeof context.idToken === 'object')
  assert(callback && typeof callback === 'function')
  // precondition: run after `ensureAccount`
  assert(user.app_metadata && typeof user.app_metadata === 'object')
  assert.strictEqual(user.app_metadata.structureVersion, 1)
  assert(
    user.app_metadata.accountId &&
      typeof user.app_metadata.accountId === 'string' &&
      accountIdPattern.test(user.app_metadata.accountId)
  )

  let logCounter = 0
  let debugCounter = 0

  // setup debug functions
  function message (str) {
    return `${context.sessionID || '--no session id--'}: ${ruleName}: ${str}`
  }

  function debug (str) {
    if (configuration[`${ruleName}-debug`]) {
      console.log(message('DEBUG (' + logCounter + '.' + debugCounter + '): ' + str))
      debugCounter++
    }
  }

  function info (str) {
    console.info(message('(' + logCounter + '): ' + str))
    logCounter++
    debugCounter = 0
  }

  /**
   * Get a request parameter from the context
   */
  function parameter (paramName) {
    // make sure 0, and the empty string from a query are not considered as it doesn't exist
    return context.request.query && context.request.query[paramName] !== undefined
      ? context.request.query[paramName]
      : context.request.body && context.request.body[paramName]
  }

  /* log call */
  info('Node Version: ' + process.version)
  debug('user: ' + JSON.stringify(user))
  debug('context: ' + JSON.stringify(context))
  const userRepresentation = user.user_id + ' (' + user.name + ', ' + user.nickname + ', ' + user.email + ')'
  const clientRepresentation = context.clientName + ' (' + context.clientID + ')'

  /* execute */
  const accountId = user.app_metadata.accountId
  info(`accountId: ${accountId}`)
  const mode = parameter(modeParameterName)
  info(`mode: ${mode}`)

  const contextMessage = `token request by ${clientRepresentation} for ${userRepresentation} with mode '${mode}' and accountId ${accountId}`
  info(contextMessage)

  context.idToken[accountIdClaim] = accountId
  context.idToken[modeClaim] = mode
  context.accessToken[accountIdClaim] = accountId
  context.accessToken[modeClaim] = mode

  // log outcome
  info(`${contextMessage} id and access tokens have claim '${accountIdClaim}: ${accountId}'`)

  return callback(null, user, context)
}
