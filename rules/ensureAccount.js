/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* global UnauthorizedError,configuration,auth0 */

/**
 * @interface User
 * @property {string} user_id
 * @property {string} sub_hashed
 * @property {string} name
 * @property {string} nickname
 * @property {string} email
 */

/**
 * @interface Request
 * @property {object} [query]
 * @property {object} [body]
 */

/**
 * @interface IdToken
 */

/**
 * @interface Context
 * @property {string} [sessionID]
 * @property {string} clientID
 * @property {string} clientName
 * @property {Request} request
 * @property {IdToken} idToken
 */

/**
 * Get extra information about the user from DynamoDB, and add it to the id token.
 *
 * @param {User} user
 * @param {Context} context
 * @param {function} callback
 */
// eslint-disable-next-line no-unused-vars
function ensureAccount (user, context, callback) {
  /* define constants */
  const ruleName = 'ensureAccount'
  const accountIdPattern = /[a-zA-Z0-9]{10}/
  const modeParameterName = 'mode' // no namespace (difficult in query)
  // keep in sync with pink-march-service//lib/ppwcode/Mode.js
  const uuidPattern = '[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}'
  const modeRegExpString = `^production|automated-test-${uuidPattern}|qa-\\d+|acceptance-\\d+|demo|june2020|dev-experiment$`
  const modePattern = new RegExp(modeRegExpString)
  const qualifiedModeRegExpString = `^automated-test-${uuidPattern}|qa-\\d+|acceptance-\\d+$`
  const qualifiedModePattern = new RegExp(qualifiedModeRegExpString)
  const AWSRegion = 'eu-west-1'
  const AWSAccessKeyIdEnvName = 'AWSaccessKeyId'
  const AWSSecretAccessKeyEnvName = 'AWSsecretAccessKey'
  /* Session token is for use in local automated tests, when assuming automated-test role.
     Do not provide this in production or on CI. */
  const AWSSessionTokenEnvName = 'AWSSessionToken'
  const tableNameTest = 'pink-march-service-test'
  const tableNameProduction = 'pink-march-service-production'
  const productionModesPattern = /^(production|demo|june2020)$/
  const auth0AsSub = `pink-march-auth0-${ruleName}`
  const thirthyDaysMs = 30 * 24 * 60 * 60 * 1000

  /* setup utilities */
  const assert = require('assert')

  assert(user && typeof user === 'object')
  assert(user.user_id && typeof user.user_id === 'string')
  assert(context && typeof context === 'object')
  assert(!context.sessionID || typeof context.sessionID === 'string')
  assert(context.request && typeof context.request === 'object')
  assert(!context.request.query || typeof context.request.query === 'object')
  assert(!context.request.body || typeof context.request.body === 'object')
  assert(context.clientID && typeof context.clientID === 'string')
  assert(context.clientName && typeof context.clientName === 'string')
  assert(context.idToken && typeof context.idToken === 'object')
  assert(callback && typeof callback === 'function')
  assert(configuration[AWSAccessKeyIdEnvName] && typeof configuration[AWSAccessKeyIdEnvName] === 'string')
  assert(configuration[AWSSecretAccessKeyEnvName] && typeof configuration[AWSAccessKeyIdEnvName] === 'string')

  let logCounter = 0
  let debugCounter = 0

  // setup debug functions
  function message (str) {
    return `${context.sessionID || '--no session id--'}: ${ruleName}: ${str}`
  }

  function debug (str) {
    if (configuration[`${ruleName}-debug`]) {
      console.log(message('DEBUG (' + logCounter + '.' + debugCounter + '): ' + str))
      debugCounter++
    }
  }

  function info (str) {
    console.info(message('(' + logCounter + '): ' + str))
    logCounter++
    debugCounter = 0
  }

  function warn (str) {
    console.warn(message('(' + logCounter + ') WARNING: ' + str))
    logCounter++
    debugCounter = 0
  }

  /**
   * Get a request parameter from the context
   */
  function parameter (paramName) {
    // make sure 0, and the empty string from a query are not considered as it doesn't exist
    return context.request.query && context.request.query[paramName] !== undefined
      ? context.request.query[paramName]
      : context.request.body && context.request.body[paramName]
  }

  function usageError (msg) {
    warn(msg)
    callback(new UnauthorizedError(message(msg)))
  }

  /* log call */
  info('Node Version: ' + process.version)
  debug('user: ' + JSON.stringify(user))
  debug('context: ' + JSON.stringify(context))
  const userRepresentation = user.user_id + ' (' + user.name + ', ' + user.nickname + ', ' + user.email + ')'
  const clientRepresentation = context.clientName + ' (' + context.clientID + ')'

  /* execute */
  const mode = parameter(modeParameterName)
  const contextMessage = `token request by ${clientRepresentation} for ${userRepresentation} with mode '${mode}'`
  info(contextMessage)

  if (!mode || typeof mode !== 'string' || !modePattern.test(mode)) {
    return usageError(`${modeParameterName} does not have a value that matches ${modePattern} in ${contextMessage}`)
  }
  info(`mode: ${mode}`)

  if (
    user.app_metadata &&
    user.app_metadata.accountId &&
    typeof user.app_metadata.accountId === 'string' &&
    accountIdPattern.test(user.app_metadata.accountId) &&
    user.app_metadata.accounts &&
    typeof user.app_metadata.accounts === 'object' &&
    user.app_metadata.accounts[mode]
  ) {
    info(
      `found existing \`accountId=${user.app_metadata.accountId}\` and account for \`mode=${mode}\` in user's \`app_metadata\`. Nothing to do.`
    )
    return callback(null, user, context)
  }

  info(
    "no good `accountId` or account for mode in user's `app_metadata`; assuming there is also no `Account` in DynamoDB."
  )

  const crypto = require('crypto') // only load when needed
  /* We assume that `36^accountIdLength` has enough entropy to not create conflicts.
     With encoding `base64', the output is [a-zA-Z0-9], which has 62 possibilities (we remove the
     occasional other characters).
     62^10 >> 10^15 */
  const accountId = crypto
    .createHash('sha256')
    .update(user.user_id)
    .digest('base64')
    .replace(/[^a-zA-Z0-9]/g, '')
    .substring(0, 10)
  if (!user.app_metadata) {
    user.app_metadata = {}
  }
  user.app_metadata.structureVersion = 1
  user.app_metadata.accountId = accountId
  info(`accountId ${accountId} stored in \`app_metadata\``)

  const key = `/${mode}/account/${accountId}`
  const now = new Date()
  const submitted = now.toISOString()

  if (!user.app_metadata.accounts) {
    user.app_metadata.accounts = {}
  }
  assert.strictEqual(typeof user.app_metadata.accounts, 'object')
  user.app_metadata.accounts[mode] = { key, submitted }

  info(`using '${key}' as \`key\` and '${submitted}' (now) as \`submitted\` for Account, stored in \`app_metadata\``)

  const cutOff = now.getTime() - thirthyDaysMs
  const oldQualifiedKeyPairModes = Object.keys(user.app_metadata.accounts).filter(m => {
    if (!qualifiedModePattern.test(m)) {
      return false
    }
    return new Date(user.app_metadata.accounts[m].submitted).getTime() < cutOff
  })
  info(`there are ${oldQualifiedKeyPairModes.length} old mode key pairs to clean up`)
  oldQualifiedKeyPairModes.forEach(m => {
    info(
      `deleting mode key pair for mode '${m}' (key: '${user.app_metadata.accounts[m].key}', submitted: '${user.app_metadata.accounts[m].submitted}')`
    )
    delete user.app_metadata.accounts[m]
  })

  // fixed version (https://auth0-extensions.github.io/canirequire/#aws-sdk)
  const awsSdkReference =
    process.env.NODE_ENV === 'mocha'
      ? 'aws-sdk'
      : /* istanbul ignore next: cannot cover code specific for Auth0 in tests - it is node-incompatible */ 'aws-sdk@2.593.0'
  const AWS = require(awsSdkReference) // only load when needed
  AWS.config.apiVersions = {
    dynamodb: '2012-08-10'
  }
  AWS.config.region = AWSRegion
  AWS.config.credentials = new AWS.Credentials(
    configuration[AWSAccessKeyIdEnvName],
    configuration[AWSSecretAccessKeyEnvName],
    configuration[AWSSessionTokenEnvName]
  )
  const dynamodb = new AWS.DynamoDB.DocumentClient()

  dynamodb
    .put({
      TableName: productionModesPattern.test(mode)
        ? /* istanbul ignore next : no automated test on production table */ tableNameProduction
        : tableNameTest,
      Item: {
        mode,
        submitted,
        submittedBy: auth0AsSub,
        key,
        data: {
          createdBy: auth0AsSub,
          createdAt: submitted,
          id: accountId,
          sub: user.user_id,
          email: user.email,
          structureVersion: 1
        }
      }
    })
    .promise()
    .then(() => {
      info('created or updated Account in DynamoDB')
      return auth0.users.updateAppMetadata(user.user_id, user.app_metadata)
    })
    .then(() => {
      // log outcome
      info(
        `${contextMessage} stored accountId ${accountId} stored in \`app_metadata\` and created / updated Account in DynamoDB)`
      )
      callback(null, user, context)
    })
    .catch(err => {
      // log outcome
      warn(
        `${contextMessage} could not persist accountId ${accountId} in \`app_metadata\`, or could not create / update Account in DynamoDB (${err.message})`
      )
      callback(null, user, context)
    })
}
