/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* global UnauthorizedError,configuration */

/**
 * @interface User
 * @property {string} user_id
 * @property {string} sub_hashed
 * @property {string} name
 * @property {string} nickname
 * @property {string} email
 */

/**
 * @interface Request
 * @property {object} [query]
 * @property {object} [body]
 */

/**
 * @interface IdToken
 */

/**
 * @interface Context
 * @property {string} [sessionID]
 * @property {string} clientID
 * @property {string} clientName
 * @property {Request} request
 * @property {IdToken} idToken
 */

/**
 * Get extra information about the user from DynamoDB, and add it to the id token.
 *
 * @param {User} user
 * @param {Context} context
 * @param {function} callback
 */
// eslint-disable-next-line
function checkAudienceAndScopes (user, context, callback) {
  /* define constants */
  const ruleName = 'checkAudienceAndScopes'
  const requiredAudience = 'https://app.pink-march.pink-ribbon-belgium.org/'
  const supportedScopes = ['openid', 'profile', 'email', 'pink-march-service:access']

  /* setup utilities */
  const assert = require('assert')

  assert(user && typeof user === 'object')
  assert(user.user_id && typeof user.user_id === 'string')
  assert(context && typeof context === 'object')
  assert(!context.sessionID || typeof context.sessionID === 'string')
  assert(context.request && typeof context.request === 'object')
  assert(!context.request.query || typeof context.request.query === 'object')
  assert(!context.request.body || typeof context.request.body === 'object')
  assert(context.clientID && typeof context.clientID === 'string')
  assert(context.clientName && typeof context.clientName === 'string')
  assert(context.idToken && typeof context.idToken === 'object')
  assert(callback && typeof callback === 'function')

  let logCounter = 0
  let debugCounter = 0

  // setup debug functions
  function message (str) {
    return `${context.sessionID || '--no session id--'}: ${ruleName}: ${str}`
  }

  function debug (str) {
    if (configuration[`${ruleName}-debug`]) {
      console.log(message('DEBUG (' + logCounter + '.' + debugCounter + '): ' + str))
      debugCounter++
    }
  }

  function info (str) {
    console.info(message('(' + logCounter + '): ' + str))
    logCounter++
    debugCounter = 0
  }

  function warn (str) {
    console.warn(message('(' + logCounter + ') WARNING: ' + str))
    logCounter++
    debugCounter = 0
  }

  /**
   * Get a request parameter from the context
   */
  function parameter (paramName) {
    // make sure 0, and the empty string from a query are not considered as it doesn't exist
    return context.request.query && context.request.query[paramName] !== undefined
      ? context.request.query[paramName]
      : context.request.body && context.request.body[paramName]
  }

  function usageError (msg) {
    warn(msg)
    callback(new UnauthorizedError(message(msg)))
  }

  function unauthorized (msg) {
    info(msg)
    callback(new UnauthorizedError(message(msg)))
  }

  /* log call */
  info('Node Version: ' + process.version)
  debug('user: ' + JSON.stringify(user))
  debug('context: ' + JSON.stringify(context))
  const userRepresentation = user.user_id + ' (' + user.name + ', ' + user.nickname + ', ' + user.email + ')'
  const clientRepresentation = context.clientName + ' (' + context.clientID + ')'

  /* execute */
  const audience = parameter('audience')
  assert(!audience || typeof audience === 'string')

  const scopeText = parameter('scope') // space separated string
  assert(!scopeText || typeof scopeText === 'string')

  const contextMessage = `token request by ${clientRepresentation} for ${userRepresentation} for audience '${audience}' and scopes '${scopeText}'`
  info(contextMessage)

  // audience is mandatory
  if (!audience) {
    return usageError(`audience is mandatory for token request, but not found in ${contextMessage}`)
  }
  info('audience found in request')

  // audience must be requiredAudience
  if (audience !== requiredAudience) {
    return unauthorized(`audience '${audience}' not supported in ${contextMessage}`)
  }
  info('audience is supported')

  // scopes must be supported
  const requestedScopes = scopeText && scopeText.split(/\s/)
  assert(!requestedScopes || Array.isArray(requestedScopes))
  if (requestedScopes && requestedScopes.some(s => !supportedScopes.includes(s))) {
    return unauthorized(`unsupported scopes in ${contextMessage} (remember scopes are space-separated)`)
  }
  info('requested scopes are supported')

  // log outcome
  info(`${contextMessage} passes audience and scopes check`)

  return callback(null, user, context)
}
