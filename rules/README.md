# Rules - Authorization Strategy

Rules are a chain-of-responsibility. Rules are run every time a user authenticates, in the sequence determined by the
`order` field in the associated `*.json` file.

_**NOTE:** The `stage` field in the associated `*.json` files is legacy, and no longer relevant. A rule can be disabled
by setting `enabled: false` in the associated `*.json` file._

When a rule _fails_, the later rules are no longer executed, and authorization fails. Rules can execute side effects, or
change or extend the OpenID Connect id token, and OAuth2 bearer access token, prepared by Auth0 before the rule chain is
executed.

Our audience is `https://app.pink-march.pink-ribbon-belgium.org/`.

Access to `pink-march-service` is requested by asking for scope `pink-march-service:access`. Apart from that, the OIDCD
scopes `openid profile email` are supported.

For custom claims, we use the namespace `https://pink-ribbon-belgium.org/`.

In our setup

1. [`ensureAccount`](ensureAccount.js) _lazily_

   - calculates a hash of the user id (`sub`), which we will use as `accountId` in our application
   - remembers the `accountId` in the user's `app_metadata` (and adds a `structureVersion`)
   - creates (or updates) an `Account` object in DynamoDB with the `sub` and `accountId` as properties

   The intention is to do this only _once_ for each user / account. We determine whether this was already done by
   checking the existence of the `accountId` in the user's `app_metadata`. By doing it lazily, if we failed the first
   time, this will be corrected later.

1. [`checkAudienceAndScopes`](checkAudienceAndScopes.js) verifies whether the `aud` and `scopes` for which tokens where
   requested are acceptable. No scopes are mandatory, but authorization fails if a scope that we do not use is
   requested.
1. [`adornTokensWithAccountIdAndMode`](adornTokensWithAccountIdAndMode.js) adds the `accountId` and `mode` as claim to
   both the id and access token
1. [`addRaas`](addRaas.js)
   - retrieves the id's of the `Group`s the account is administrator and member of from DynamoDB
   - builds the RAAs that authorize the user in the service for particular Resource Actions,
   - and adds those RAAs as claim to the access token

We use a hash of the user id `accountId` in our application in URIs for which the account is relevant. This is because
in some cases, this URI is used by other users (e.g., to see the account's public profile), and it contains some
information that is better kept secret from other accounts. Notably, it shows which social profile was used to create
the account in the first place. The `email`, e.g., cannot be used, because it might change. The user id (`sub`) _is_
used for internal auditing.

[chain-of-responsibility]: https://en.wikipedia.org/wiki/Chain-of-responsibility_pattern
