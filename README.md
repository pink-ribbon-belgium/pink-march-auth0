# Pink Ribbon Belgium - Pink March 2020 Auth0 Configuration - Rules and Pages

Auth0 configuration of the Pink Ribbon Belgium Pink March 2020 application.

Authentication and Authorization for Pink Ribbon Belgium Pink March 2020 services is controlled by [Auth0], which we use
as Identity Provider (IdP), Security Token Service (STS), and Authorization Service.

Customization _inside_ [Auth0] is done using the code in this repository. It holds

- [Rules], i.e., JavaScript that is executed on a login, token request, etc.
- HTML [Pages] that replace the off-the-shelf pages of Auth0 for a login, password reset, etc.
- [Database Connections] that provide a way for [Rules] to access a database

[Hooks] will eventually replace [Rules]. [Hooks] is still in beta (and has been for a long time). Auth0 says it will
implement new functionality in [Hooks]. [Hooks] are needed for the OAuth2 `client credentials` flow, which we will not
use. It is expensive with [Auth0]. We only need this for automated tests, and replace this with the less expensive
OAuth2 "Username Password" flow for this purpose. Therefor, we do not use hooks.

For details about the API, see

- [Rules]
- [Pages]

[Rules] can be executed in the stages `login_success`, `login_failure`, or `user_registration`.

## Authorization

In access tokens returned by the STS service, we add the necessary information for authorization, which includes the ids
of teams, organizations, … the user can administer, and the team, organization, … she is in.

This information is managed in a DynamoDB table in the Pink Ribbon Belgium AWS account, and read by a [Rule]] defined
here, with an IAM identity with "least privileges".

## Target environment

This application is developed for Node 12 LTS or later.

The editor in the Auth0 UI has its own mind about style. It is not Standard, in any case. Code deployed via this
repository is.

## Development

### IDE

JetBrains WebStorm settings are included and kept up-to-date by all developers.

### Development and Testing

Development and Testing is a bit harder, because the JavaScript files for [Rules] need to contain a naked function for
Auth0, and cannot be a CommonJS module.

To test the code of [Rules], we need to load the file, and `eval` it. This makes debugging impossible. Furthermore,
style and syntax checking have trouble with this approach, and are largely disabled.

To debug the deployed version, see the [Rules] in the console, edit the Rule in question, and follow the description you
get when you click 'Debug Rule' (at the bottom of the page).

Our functions log at debug level when you add the appropriate configuration parameter in the main [Rules] page of the
console. The key is `<rule name>-debug`. Any none-falsy value enables debug logging. To stop debug logging, remove the
configuration parameter.

## Performance

Execution of the entire [Rules] chain seems limited by Auth0 to 2.5s.

## Deployment

These scripts are [deployed automatically] from the Bitbucket repository `production` branch to the auth0 tenant
`rozemars`. For development, they are [deployed automatically] from the Bitbucket repository `development` branch to the
auth0 tenant `rozemars-dev`.

**Note that this is always a full deployment of the contents of this repository. Any rules or database connection
scripts that exist in Auth0 but not in this repository will be deleted, unless they are marked as _manual_.**

The [Rules] and [Pages] are deployed using the Bitbucket - Auth0 integration.

## License

Released under the [GNU GENERAL PUBLIC LICENSE, Version 3](https://www.gnu.org/licenses/gpl-3.0.nl.html).

[auth0]: https://manage.auth0.com/
[rules]: https://auth0.com/docs/rules/current
[pages]: https://auth0.com/docs/hosted-pages
[database connections]: https://auth0.com/docs/connections/database/custom-db
[hooks]: https://auth0.com/docs/hooks
[deployed automatically]: https://auth0.com/docs/extensions/bitbucket-deploy
