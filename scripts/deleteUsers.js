const fetch = require('node-fetch')
const path = require('path')
let config

this.auth0Domain = 'value is set when initializing depending on the config'
this.myClientId = 'value is set when initializing depending on the config'
this.myClientSecret = 'value is set when initializing depending on the config'
this.myAudience = 'value is set when initializing depending on the config'

// eslint-disable-next-line no-template-curly-in-string
// const companyVariable = '${companyName}'

const help = `
Run this script with argument \`<AUTH0_CONFIG>\` (dev or prod).`

if (process.argv.includes('-h')) {
  console.log(help)
  process.exit(0)
}
const auth0Config = process.argv[2]
if (auth0Config !== 'dev' && auth0Config !== 'prod') {
  console.error('INVALID CONFIG => use dev or prod')
  console.error(help)
  process.exit(-1)
}

/**
 * Make sure that the script uses the correct environment
 * which is given by the parameter mode at startup
 */
function requireCorrectConfig () {
  if (!config) {
    process.env.NODE_CONFIG_DIR = path.join(__dirname, '..', 'config')

    config = require('config')
  }
}

async function getAccessToken () {
  const body = {
    client_id: this.myClientId,
    client_secret: this.myClientSecret,
    audience: this.myAudience,
    grant_type: 'client_credentials'
  }
  const tokenUrl = `https://${this.auth0Domain}/oauth/token`
  const response = await fetch(tokenUrl, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: { 'content-type': 'application/json' }
  })

  const json = await response.json()
  console.log('token', response.status)
  const accessToken = json.access_token

  if (accessToken === undefined) {
    throw new Error('Invalid Acces Token')
  }

  return accessToken
}

async function getUsers (accessToken) {
  console.log()
  const url = `https://${this.auth0Domain}/api/v2/users?per_page=100`

  const response = await fetch(url, {
    method: 'get',
    headers: {
      'content-type': 'application/json',
      authorization: `Bearer ${accessToken}`
    }
  })
  console.log('getUsers', response.status)

  const result = await response.json()
  return result
}

async function deleteUser (accessToken, userId) {
  const url = `https://${this.auth0Domain}/api/v2/users/${userId}`
  const response = await fetch(url, {
    method: 'delete',
    headers: {
      'content-type': 'text/html',
      authorization: `Bearer ${accessToken}`
    }
  })
  console.log('deleteUsers', response.status)
}

async function setConfigVariables (auth0Config) {
  switch (auth0Config) {
    case 'dev':
    case 'prod':
      this.auth0Domain = config.auth0_text_management[auth0Config].auth0Domain
      this.myClientId = config.auth0_text_management[auth0Config].clientId
      this.myClientSecret = config.auth0_text_management[auth0Config].clientSecret
      this.myAudience = `https://${config.auth0_text_management[auth0Config].auth0Domain}/api/v2/`
      break
    default:
      throw new Error('INVALID CONFIG')
  }
}

const sleep = milliseconds => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

/***
 * Apply custom text to the Auth0 dialogs
 * @param auth0Config which config should be used (dev or prod)
 * @returns {Promise<void>}
 */
async function execute (auth0Config) {
  let users
  requireCorrectConfig()

  await setConfigVariables(auth0Config)

  const accessToken = await getAccessToken()

  users = await getUsers(accessToken)

  // While loop is used for production
  // timeouts are set to stay within rate limit
  while (users.length > 0) {
    // TODO should be refactored for better performance
    for (const user of users) {
      await sleep(300)
      await deleteUser(accessToken, user.user_id)
    }
    console.log('deleted users')
    // accessToken = await getAccessToken()
    await sleep(6000)
    users = await getUsers(accessToken)
  }

  // used for dev
  // users.map(async user => {
  //   const userId = user.user_id
  //   await deleteUser(accessToken, userId)
  // })
}

execute(auth0Config).catch(err => {
  console.error(`Update Auth0 texts failed: ${err}`)
  console.error(err)
  process.exit(-1)
})
