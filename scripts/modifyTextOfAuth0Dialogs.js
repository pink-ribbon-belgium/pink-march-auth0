const fetch = require('node-fetch')
const path = require('path')
let config

this.auth0Domain = 'value is set when initializing depending on the config'
this.myClientId = 'value is set when initializing depending on the config'
this.myClientSecret = 'value is set when initializing depending on the config'
this.myAudience = 'value is set when initializing depending on the config'

// eslint-disable-next-line no-template-curly-in-string
const companyVariable = '${companyName}'

const help = `
Run this script with argument \`<AUTH0_CONFIG>\` (dev or prod).`

if (process.argv.includes('-h')) {
  console.log(help)
  process.exit(0)
}
const auth0Config = process.argv[2]
if (auth0Config !== 'dev' && auth0Config !== 'prod') {
  console.error('INVALID CONFIG => use dev or prod')
  console.error(help)
  process.exit(-1)
}

/**
 * Make sure that the script uses the correct environment
 * which is given by the parameter mode at startup
 */
function requireCorrectConfig () {
  if (!config) {
    process.env.NODE_CONFIG_DIR = path.join(__dirname, '..', 'config')

    config = require('config')
  }
}

async function getAccessToken () {
  const body = {
    client_id: this.myClientId,
    client_secret: this.myClientSecret,
    audience: this.myAudience,
    grant_type: 'client_credentials'
  }

  const tokenUrl = `https://${this.auth0Domain}/oauth/token`
  const response = await fetch(tokenUrl, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: { 'content-type': 'application/json' }
  })

  const json = await response.json()
  const accessToken = json.access_token

  if (accessToken === undefined) {
    throw new Error('Invalid Acces Token')
  }

  return accessToken
}

async function applyPrompt (accessToken, prompt, language, body) {
  const url = `https://${this.auth0Domain}/api/v2/prompts/${prompt}/custom-text/${language}`

  const response = await fetch(url, {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      'content-type': 'application/json',
      authorization: `Bearer ${accessToken}`
    }
  })

  const result = await response.json()
  console.log(result)
}

async function applyLoginText (accessToken) {
  console.log()

  const contentEn = {
    login: {
      title: 'Login',
      pageTitle: `Login to ${companyVariable}`,
      description: `Login to continue to ${companyVariable} or scroll down to Sign up for a new account.`
    }
  }

  const contentNl = {
    login: {
      title: 'Aanmelden',
      pageTitle: `Aanmelden bij ${companyVariable}`,
      description: `Meld u aan bij ${companyVariable} of scroll naar beneden om een nieuw account aan te maken.`,
      footerLinkText: 'Nieuw account aanmaken'
    }
  }

  const contentFr = {
    login: {
      title: 'Se Connecter',
      pageTitle: 'Inscrire à La Marche Rose',
      description: 'Inscrivez-vous à La Marche Rose ou faites défiler vers le bas pour créer un nouveau compte.',
      footerLinkText: 'Créer un nouveau compte'
    }
  }

  await applyPrompt(accessToken, 'login', 'en', contentEn)
  await applyPrompt(accessToken, 'login', 'nl', contentNl)
  await applyPrompt(accessToken, 'login', 'fr', contentFr)
}

async function applySignUpText (accessToken) {
  const contentNl = {
    signup: {
      title: 'Nieuw account',
      pageTitle: `Nieuw account aanmaken bij ${companyVariable}`,
      description: `Maak een nieuw account aan bij ${companyVariable} of scroll naar beneden indien u reeds een account hebt.`,
      footerLinkText: 'Aanmelden'
    }
  }

  const contentFr = {
    signup: {
      title: 'Nouveau compte',
      pageTitle: 'Créer un nouveau compte à La Marche Rose',
      description:
        'Créez un nouveau compte à La Marche Rose ou faites défiler vers le bas si vous avez déjà un compte.',
      footerLinkText: 'Inscrire'
    }
  }

  await applyPrompt(accessToken, 'signup', 'nl', contentNl)
  await applyPrompt(accessToken, 'signup', 'fr', contentFr)
}

async function applyCustomTemplateUniversalLogin (accessToken) {
  const customTemplate =
    '<!DOCTYPE html><html>' +
    '  <head>' +
    '    {%- auth0:head -%}' +
    '    <style>' +
    '      .footer {' +
    '        background-color: rgb(120, 120, 120);' +
    '        position: absolute;' +
    '        bottom: 0;' +
    '        left: 0;' +
    '        width: 100%;' +
    '        color: white;' +
    '        /* Use a high z-index for future-proofing */' +
    '        z-index: 10;' +
    '      }' +
    '      .footer ul {' +
    '        text-align: center;' +
    '      }' +
    '      .footer ul li {' +
    '        display: inline-block;' +
    '        margin: 0 4px;' +
    '      }' +
    '      .footer ul li:not(:first-of-type) {' +
    '        margin-left: 0;' +
    '      }' +
    '      .footer ul li:not(:first-of-type)::before {' +
    "        content: '';" +
    '        display: inline-block;' +
    '        vertical-align: middle;' +
    '        width: 4px;' +
    '        height: 4px;' +
    '        margin-right: 4px;' +
    '        background-color: white;' +
    '        border-radius: 50%;' +
    '      }' +
    '      .footer a {' +
    '        color: white;' +
    '      }' +
    '    </style>' +
    '     <title>{{ prompt.screen.texts.pageTitle }}</title>' +
    '  </head>' +
    '  <body class="_widget-auto-layout">' +
    '    {%- auth0:widget -%}' +
    '    <footer class="footer">' +
    '      <ul>' +
    '        <li><a href="https://www.derozemars.be/privacy-policy" target="_blank">Privacy Policy</a></li>' +
    '        <li><a href="https://www.derozemars.be/cookie-policy/" target="_blank">Cookie Policy</a></li>' +
    '        <li><a href="https://www.derozemars.be/terms-of-use/" target="_blank">Terms of Use</a></li>' +
    '      </ul>' +
    '    </footer>' +
    '  </body></html>'

  const url = `https://${this.auth0Domain}/api/v2/branding/templates/universal-login`

  const response = await fetch(url, {
    method: 'PUT',
    body: customTemplate,
    headers: {
      'content-type': 'text/html',
      authorization: `Bearer ${accessToken}`
    }
  })

  console.log(response.status)
}

async function setConfigVariables (auth0Config) {
  switch (auth0Config) {
    case 'dev':
    case 'prod':
      this.auth0Domain = config.auth0_text_management[auth0Config].auth0Domain
      this.myClientId = config.auth0_text_management[auth0Config].clientId
      this.myClientSecret = config.auth0_text_management[auth0Config].clientSecret
      this.myAudience = `https://${config.auth0_text_management[auth0Config].auth0Domain}/api/v2/`
      break
    default:
      throw new Error('INVALID CONFIG')
  }
}

/***
 * Apply custom text to the Auth0 dialogs
 * @param auth0Config which config should be used (dev or prod)
 * @returns {Promise<void>}
 */
async function execute (auth0Config) {
  requireCorrectConfig()

  await setConfigVariables(auth0Config)

  const accessToken = await getAccessToken()

  await applyLoginText(accessToken)
  await applySignUpText(accessToken)

  await applyCustomTemplateUniversalLogin(accessToken)
}

execute(auth0Config).catch(err => {
  console.error(`Update Auth0 texts failed: ${err}`)
  console.error(err)
  process.exit(-1)
})
