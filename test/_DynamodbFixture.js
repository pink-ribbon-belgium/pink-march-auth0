/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const AWS = require('aws-sdk')
const awsTestCredentials = require('./_awsTestCredentials')

const TableName = 'pink-march-service-test'

class DynamodbFixture {
  constructor () {
    this.keyCouples = []
    this.dynamodbInstancePromise = undefined // cannot get it async in constructor
  }

  ensureDynamodbInstance () {
    if (!this.dynamodbInstancePromise) {
      AWS.config.apiVersions = {
        dynamodb: '2012-08-10'
      }
      AWS.config.region = 'eu-west-1'
      this.dynamodbInstancePromise = awsTestCredentials().then(credentials => {
        AWS.config.credentials = new AWS.Credentials(
          credentials.accessKeyId,
          credentials.secretAccessKey,
          credentials.sessionToken
        )
        return new AWS.DynamoDB.DocumentClient()
      })
    }
    return this.dynamodbInstancePromise
  }

  get (key, submitted) {
    return this.ensureDynamodbInstance()
      .then(dynamodb => {
        return dynamodb
          .query({
            TableName,
            ExpressionAttributeNames: {
              '#key': 'key',
              '#submitted': 'submitted'
            },
            ExpressionAttributeValues: {
              ':key': key,
              ':submitted': submitted
            },
            KeyConditionExpression: '#key = :key and #submitted = :submitted',
            Limit: 1,
            ConsistentRead: true
          })
          .promise()
      })
      .then(result => {
        this.keyCouples.push({ key, submitted })
        return result.Items[0]
      })
  }

  hasArrived () {
    return Promise.all(
      this.keyCouples.map(keyCouple =>
        this.get(keyCouple.key, keyCouple.submitted).then(result => {
          console.info(`item ${util.inspect(result)} has arrived`)
        })
      )
    ).then(() => {
      console.info('all items have arrived')
    })
  }

  putItem (Item) {
    return this.ensureDynamodbInstance()
      .then(dynamodb => {
        return dynamodb
          .put({
            TableName,
            Item
          })
          .promise()
      })
      .then(() => {
        console.log(`put item ${util.inspect(Item)}, waiting for arrival …`)
        this.keyCouples.push({ key: Item.key, submitted: Item.submitted })
        return this.hasArrived()
      })
  }

  async clean () {
    return this.ensureDynamodbInstance().then(dynamodb =>
      Promise.all(
        this.keyCouples.map(keyCouple =>
          dynamodb
            .delete({
              TableName,
              Key: keyCouple
            })
            .promise()
            .then(() => {
              console.info(`deleted item ${util.inspect(keyCouple)}`)
            })
            .catch(() => {
              console.info(`failed to delete ${util.inspect(keyCouple)}`)
            })
        )
      ).then(() => {
        console.info('deleted all items')
      })
    )
  }
}

module.exports = DynamodbFixture
