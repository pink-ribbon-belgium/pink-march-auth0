/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('./_testName')(module)
const should = require('should')
const testSourceSyntax = require('./_testSourceSyntax')
const commonFixtureSetup = require('./_commonFixtureSetup')
const setupBasicUserAndContextAndUnauthorizedTest = require('./_setupBasicUserAndContextAndUnauthorizedTest')
const Hoek = require('@hapi/hoek')

const common = require('./_common')('adornTokensWithAccountIdAndMode')

describe(testName, function () {
  testSourceSyntax(common)

  describe('runs with', function () {
    commonFixtureSetup(module, common)

    const parameterLocations = ['query', 'body']
    parameterLocations.forEach(parameterLocation => {
      describe(parameterLocation, function () {
        setupBasicUserAndContextAndUnauthorizedTest(common, parameterLocation)

        function itWorks (fixture, done) {
          // noinspection SpellCheckingInspection
          const accountId = 'abcdefghij'
          fixture.user.app_metadata.structureVersion = 1
          fixture.user.app_metadata.accountId = accountId
          fixture.user.app_metadata.accountId.should.match(common.accountIdPattern)
          const mode = fixture.context.request[parameterLocation].mode
          const oldUser = Hoek.clone(fixture.user)
          const oldContext = Hoek.clone(fixture.context)
          fixture.subject(fixture.user, fixture.context, (err, user, context) => {
            try {
              should(err).not.be.ok()
              user.should.deepEqual(oldUser)
              context.idToken[common.accountIdClaim].should.equal(accountId)
              context.idToken[common.modeClaim].should.equal(mode)
              context.accessToken[common.accountIdClaim].should.equal(accountId)
              context.accessToken[common.modeClaim].should.equal(mode)
              oldContext.idToken[common.accountIdClaim] = accountId
              oldContext.idToken[common.modeClaim] = mode
              oldContext.accessToken[common.accountIdClaim] = accountId
              oldContext.accessToken[common.modeClaim] = mode
              context.should.deepEqual(oldContext)
              done()
            } catch (e) {
              done(e)
            }
          })
        }

        it('adds the accountId and mode to both the id and access token', function (done) {
          itWorks(this, done)
        })
        it('runs without a sessionID', function (done) {
          delete this.context.sessionID
          itWorks(this, done)
        })
        it('runs without debug mode', function (done) {
          // covers a branch we otherwise miss
          global.configuration[common.debugPropertyName] = false
          itWorks(this, done)
        })
        const otherParameterLocations = parameterLocations.filter(pl => pl !== parameterLocation)
        otherParameterLocations.forEach(opl => {
          it(`runs without a ${opl}`, function (done) {
            delete this.context.request[opl]
            itWorks(this, done)
          })
        })
      })
    })
  })
})
