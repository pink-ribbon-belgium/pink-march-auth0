/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('./_testName')(module)
const should = require('should')
const testSourceSyntax = require('./_testSourceSyntax')
const commonFixtureSetup = require('./_commonFixtureSetup')
const setupBasicUserAndContextAndUnauthorizedTest = require('./_setupBasicUserAndContextAndUnauthorizedTest')
const crypto = require('crypto') // only load when needed
const Hoek = require('@hapi/hoek')
const DynamoDBFixture = require('./_DynamodbFixture')
/* NOTE: aws-sdk-mock works with recent versions of AWS SDK, but not with the older one imposed by Auth0.
         See test below. That makes it impossible to cover the final catch.
const AWSMock = require('aws-sdk-mock')
*/

const common = require('./_common')('ensureAccount')

function accountId (userId) {
  return crypto
    .createHash('sha256')
    .update(userId)
    .digest('base64')
    .replace(/[^a-zA-Z0-9]/g, '')
    .substring(0, common.accountIdLength)
}

describe(testName, function () {
  it('we have chosen a sensible hash algorithm', function () {
    const s = 'This is some random text'
    const result = accountId(s)
    console.log(result)
    result.should.match(common.accountIdPattern)
  })

  testSourceSyntax(common)

  describe('runs with', function () {
    commonFixtureSetup(module, common)

    const parameterLocations = ['query', 'body']
    parameterLocations.forEach(parameterLocation => {
      describe(parameterLocation, function () {
        setupBasicUserAndContextAndUnauthorizedTest(common, parameterLocation, false, true)

        function itWorks (fixture, done, expectedNoChange, expectedNoDynamodbSuccess, deletedKeyPairModes) {
          const oldUser = Hoek.clone(fixture.user)
          const oldContext = Hoek.clone(fixture.context)
          const expectedAccountId = accountId(fixture.user.user_id)
          const mode = fixture.context.request[parameterLocation].mode
          const expectedKey = `/${mode}/account/${expectedAccountId}`
          const submittedBy = 'pink-march-auth0-ensureAccount'
          const expected = {
            submittedBy,
            // submitted: TBD
            key: expectedKey,
            mode: mode,
            data: {
              createdBy: submittedBy,
              // no mode here!
              // createdAt: TBD
              id: expectedAccountId,
              sub: fixture.user.user_id,
              email: fixture.user.email,
              structureVersion: 1
            }
          }

          fixture.subject(fixture.user, fixture.context, (err, user, context) => {
            try {
              should(err).not.be.ok()
              user.app_metadata.accountId.should.match(common.accountIdPattern)
              if (!expectedNoChange) {
                user.app_metadata.structureVersion.should.equal(1)
                user.app_metadata.accountId.should.equal(expectedAccountId)
                user.app_metadata.accounts.should.be.an.Object()
                user.app_metadata.accounts[mode].should.be.an.Object()
                user.app_metadata.accounts[mode].key.should.equal(expectedKey)
                const submitted = user.app_metadata.accounts[mode].submitted
                submitted.should.be.a.String() // ISO-8601
                if (!oldUser.app_metadata) {
                  oldUser.app_metadata = {}
                }
                oldUser.app_metadata.structureVersion = 1
                oldUser.app_metadata.accountId = expectedAccountId
                if (!oldUser.app_metadata.accounts) {
                  oldUser.app_metadata.accounts = {}
                }
                oldUser.app_metadata.accounts[mode] = {
                  key: expectedKey,
                  submitted
                }
                if (expectedNoDynamodbSuccess) {
                  return done()
                }
                if (deletedKeyPairModes) {
                  deletedKeyPairModes.forEach(m => {
                    fixture.user.app_metadata.accounts.should.not.have.ownProperty(m)
                    delete oldUser.app_metadata.accounts[m]
                  })
                }
                expected.submitted = submitted
                expected.data.createdAt = submitted
                fixture.dynamodbFixture
                  .get(expectedKey, submitted)
                  .then(result => {
                    console.log(result)
                    result.should.deepEqual(expected)
                    user.should.deepEqual(oldUser)
                    context.should.deepEqual(oldContext)
                    done()
                  })
                  .catch(e => done(e))
              } else {
                user.should.deepEqual(oldUser)
                context.should.deepEqual(oldContext)
                done()
              }
            } catch (e) {
              done(e)
            }
          })
        }

        it('does nothing when there already is a good accountId and account for the mode', function (done) {
          this.user.app_metadata.accountId = '0123456789'
          const mode = this.context.request[parameterLocation].mode
          this.user.app_metadata.accounts = {}
          this.user.app_metadata.accounts[mode] = {}
          itWorks(this, done, true)
        })
        it('runs without a sessionID', function (done) {
          this.user.app_metadata.accountId = '0123456789'
          const mode = this.context.request[parameterLocation].mode
          this.user.app_metadata.accounts = {}
          this.user.app_metadata.accounts[mode] = {}
          delete this.context.sessionID
          itWorks(this, done, true)
        })
        const otherParameterLocations = parameterLocations.filter(pl => pl !== parameterLocation)
        otherParameterLocations.forEach(opl => {
          it(`runs without a ${opl}`, function (done) {
            this.user.app_metadata.accountId = '0123456789'
            const mode = this.context.request[parameterLocation].mode
            this.user.app_metadata.accounts = {}
            this.user.app_metadata.accounts[mode] = {}
            delete this.context.request[opl]
            itWorks(this, done, true)
          })
        })
        it('throws UnauthorizedError without a query and without a body', function (done) {
          delete this.context.request.query
          delete this.context.request.body
          this.shouldBeUnauthorized(
            this.user,
            this.context,
            [new RegExp(common.modeParameterName), /does not have a value that matches \/.*\/ in/],
            done
          )
        })
        it('throws UnauthorizedError without a mode', function (done) {
          delete this.context.request[parameterLocation].mode
          this.shouldBeUnauthorized(
            this.user,
            this.context,
            [new RegExp(common.modeParameterName), /does not have a value that matches \/.*\/ in/],
            done
          )
        })

        describe('writes to dynamodb', function () {
          beforeEach(function () {
            this.dynamodbFixture = new DynamoDBFixture()
          })

          afterEach(function () {
            this.dynamodbFixture.clean()
          })

          it('calculates and sets an account id when there is none yet, and creates or updates Account', function (done) {
            delete this.user.app_metadata.accountId
            itWorks(this, done)
          })
          it('calculates and sets an account id when there is no app_metadata, and creates or updates Account', function (done) {
            delete this.user.app_metadata
            itWorks(this, done)
          })

          const things = [1, {}, [], true]

          things.forEach(t => {
            it(`calculates and sets an account id when the accountId is not a string but type ${typeof t}, creates or updates Account, and remembers the keys with the mode`, function (done) {
              this.user.app_metadata.accountId = t
              itWorks(this, done)
            })
          })
          it('calculates and sets an account id when the accountId is a string that does not follow the pattern, creates or updates Account, and remembers the keys with the mode', function (done) {
            this.user.app_metadata.accountId = 'something else'
            itWorks(this, done)
          })
          it('calculates and sets an account id when there is an accountId, and app_metadata.accounts exists, creates or updates Account, and remembers the keys with the mode', function (done) {
            this.user.app_metadata.accounts = {}
            itWorks(this, done)
          })
          it('runs without debug mode', function (done) {
            // covers a branch we otherwise miss
            global.configuration[common.debugPropertyName] = false
            itWorks(this, done)
          })
          it('cleans up old mode key pairs', function (done) {
            const deleteModes = ['automated-test-91575476-e6ca-48a8-8db9-f675bef8d05e', 'qa-00123', 'acceptance-00123']
            const futureQualifiedModes = [
              'automated-test-0c6dbb79-81a2-483f-b536-13fdbf92386a',
              'qa-00456',
              'acceptance-00456'
            ]
            const keepModes = ['production', 'demo', 'june2020', 'dev-expermiment']
            const oldSubmitted = '1111-03-02T10:18:36.509Z'
            const futureSubmitted = '2222-03-02T10:18:36.509Z'
            this.user.app_metadata.accounts = {}
            deleteModes.forEach(m => {
              this.user.app_metadata.accounts[m] = { key: 'dummy', submitted: oldSubmitted }
            })
            keepModes.forEach(m => {
              this.user.app_metadata.accounts[m] = { key: 'dummy', submitted: oldSubmitted }
            })
            futureQualifiedModes.forEach(m => {
              this.user.app_metadata.accounts[m] = { key: 'dummy', submitted: futureSubmitted }
            })
            itWorks(this, done, false, false, deleteModes)
          })
        })
        describe('auth0.users.updateAppMetadata errors', function () {
          it('deals with AWS/app_metadata update errors silently', function (done) {
            global.auth0.users.updateAppMetadata.rejects(new Error('Mock auth0.users.updateAppMetadata error'))
            itWorks(this, done, false, true)
          })
        })
        /* NOTE: Below test works with recent versions of AWS SDK, but not with the older one imposed by Auth0
         That makes it impossible to cover the final catch.
        describe('aws errors', function () {
          beforeEach(function () {
            this.error = new Error('Mock dynamodb error')
            AWSMock.mock('DynamoDB.DocumentClient', 'put', (params, callback) => {
              callback(this.error)
            })
          })
          afterEach(function () {
            AWSMock.restore('DynamoDB.DocumentClient')
          })

          it('deals with AWS errors silently', function (done) {
            itWorks(this, done, false, true)
          })
        })
        */
      })
    })
  })
})
