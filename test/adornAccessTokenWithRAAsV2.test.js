/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('./_testName')(module)
const should = require('should')
const testSourceSyntax = require('./_testSourceSyntax')
const commonFixtureSetup = require('./_commonFixtureSetup')
const setupBasicUserAndContextAndUnauthorizedTest = require('./_setupBasicUserAndContextAndUnauthorizedTest')
const Hoek = require('@hapi/hoek')
const mm = require('micromatch')
const DynamodbFixture = require('./_DynamodbFixture')

const common = require('./_common')('adornAccessTokenWithRAAs')

const sub = 'ihjht903lg39wtpo39'
const createdAt = '2020-02-28T13:53:18.267Z'
const actual = 'actual'
const accountId = 'abcdefghij'

const teamIds = ['bcbb8e66-2a05-414c-a395-e8d0dec1e008', '7a3f8088-b8ce-4ec9-83aa-e0e0b433c0ee']
const companyIds = ['b36583a9-b705-4cf4-8a85-3b453c856b5e', '6f6dc26a-beed-4e12-9c89-2e1b4be4c878']

function createGroupAdministrationItem (type, mode, groupId) {
  const key = `/${mode}/groupAdministration/account/${accountId}/group/${groupId}`
  return {
    mode: mode,
    submitted: actual,
    submittedBy: sub,
    key,
    partition_key_A: `/${mode}/account/${accountId}/administratorOf`,
    sort_key_A: actual,
    partition_key_B: `/${mode}/group/${groupId}/administrators`,
    sort_key_B: actual,
    data: {
      mode: mode,
      createdBy: sub,
      createdAt,
      groupType: type,
      accountId,
      groupId,
      deleted: false,
      structureVersion: 1
    }
  }
}

const expectedAuthorized = [
  'GET:/I/health',
  `GET:/I/account/${accountId}`,
  `GET:/I/account/${accountId}/publicProfile`,
  `GET:/I/account/${accountId}/preferences`,
  `GET:/I/account/${accountId}/slot`,
  'GET:/I/team/e5733fb9-97d5-4087-a172-6ad418f3f72f/publicProfile',
  'GET:/I/team/e5733fb9-97d5-4087-a172-6ad418f3f72f/verification',
  `PUT:/I/team/e5733fb9-97d5-4087-a172-6ad418f3f72f/claimSlot/${accountId}`,
  'GET:/I/company/e5733fb9-97d5-4087-a172-6ad418f3f72f/publicProfile',
  'GET:/I/company/e5733fb9-97d5-4087-a172-6ad418f3f72f/verification',
  `PUT:/I/company/e5733fb9-97d5-4087-a172-6ad418f3f72f/claimSlot/${accountId}`,
  'POST:/I/payment/calculation',
  'POST:/I/team',
  'POST:/I/company'
]

const expectedAuthorizedAdministratorTeam = expectedAuthorized.concat(
  teamIds.reduce(
    (acc, id) =>
      acc.concat([
        `GET:/I/team/${id}`,
        `PUT:/I/team/${id}`,
        `GET:/I/team/${id}/publicProfile`,
        `PUT:/I/team/${id}/publicProfile`,
        `GET:/I/team/${id}/administrators/anAccountId`,
        `PUT:/I/team/${id}/administrators/anAccountId`,
        `GET:/I/team/${id}/slot/aSlotId`,
        `PUT:/I/team/${id}/slot/aSlotId`,
        `POST:/I/team/${id}/payment`,
        `GET:/I/team/${id}/payments`,
        `PUT:/I/team/${id}/payment/7ab1d175-8b26-44e2-9baf-48fbe5c7ea72/process/${accountId}`,
        `GET:/I/team/${id}/slots`
      ]),
    []
  )
)

const expectedAuthorizedAdministratorCompany = expectedAuthorized.concat(
  companyIds.reduce(
    (acc, id) =>
      acc.concat([
        `GET:/I/company/${id}`,
        `PUT:/I/company/${id}`,
        `GET:/I/company/${id}/publicProfile`,
        `PUT:/I/company/${id}/publicProfile`,
        `GET:/I/company/${id}/administrators/anAccountId`,
        `PUT:/I/company/${id}/administrators/anAccountId`,
        `GET:/I/company/${id}/slot/aSlotId`,
        `PUT:/I/company/${id}/slot/aSlotId`,
        `POST:/I/company/${id}/payment`,
        `GET:/I/company/${id}/payments`,
        `PUT:/I/company/${id}/payment/7ab1d175-8b26-44e2-9baf-48fbe5c7ea72/process/${accountId}`,
        `GET:/I/company/${id}/slots`
      ]),
    []
  )
)

const expectedNotAuthorized = [
  'DELETE:anything',
  'HEAD:anything',
  'OPTIONS:anything',
  'PATCH:anything',
  'GET:/I/account/kskdk34rfg',
  'GET:/I/account/kskdk34rfg/something',
  'GET:/I/account/kskdk34rfg/something/else',
  'PUT:/I/account/kskdk34rfg/something',
  'PUT:/I/account/kskdk34rfg/something/else',
  `PUT:/I/account/${accountId}/something`,
  `PUT:/I/account/${accountId}/something/else`,
  'PUT:/I/account/kskdk34rfg',
  'GET:/I/account',
  'GET:/I/account/',
  'PUT:I/account/kskdk34rfg',
  'GET:/account/kskdk34rfg',
  'GET:/II/account/kskdk34rfg',
  'PUT:/I/team/e5733fb9-97d5-4087-a172-6ad418f3f72f/publicProfile',
  'GET:/I/team/e5733fb9-97d5-4087-a172-6ad418f3f72f/something',
  'GET:/I/team/e5733fb9-97d5-4087-a172-6ad418f3f72f/something/else',
  'GET:/I/team/e5733fb9-97d5-4087-a172-6ad418f3f72f/slots',
  'GET:/I/payment/calculation',
  'PUT:/I/payment/calculation',
  'PUT:/I/payment/e5733fb9-97d5-4087-a172-6ad418f3f72f',
  'PUT:/I/payment/e5733fb9-97d5-4087-a172-6ad418f3f72f/',
  'PUT:/I/payment/e5733fb9-97d5-4087-a172-6ad418f3f72f/something',
  'PUT:/I/payment/e5733fb9-97d5-4087-a172-6ad418f3f72f/something/else',
  'GET:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce',
  'PUT:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce',
  'POST:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce/payment',
  'PUT:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce/publicProfile',
  'GET:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce/administrators',
  'PUT:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce/administrators',
  'GET:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce/administrators/',
  'PUT:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce/administrators/',
  'GET:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce/administrators/mike',
  'PUT:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce/administrators/mike',
  'GET:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce/administrators/mike/',
  'PUT:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce/administrators/mike/',
  'GET:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce/administrators/mike/dick',
  'PUT:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce/administrators/mike/dick',
  `PUT:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce/payment/7ab1d175-8b26-44e2-9baf-48fbe5c7ea72/${accountId}`,
  'PUT:/I/team/43dcacfd-9b50-458e-9a36-97d3ca0ca8ce/claimSlot/this-is-not-me'
]

describe(testName, function () {
  testSourceSyntax(common)

  describe('runs', function () {
    commonFixtureSetup(module, common)

    const parameterLocations = ['query', 'body']
    parameterLocations.forEach(parameterLocation => {
      describe(parameterLocation, function () {
        setupBasicUserAndContextAndUnauthorizedTest(common, parameterLocation)

        function itWorks (fixture, done, administrator, groupType) {
          fixture.user.app_metadata.structureVersion = 1
          fixture.user.app_metadata.accountId = accountId
          fixture.user.app_metadata.accountId.should.match(common.accountIdPattern)
          const oldUser = Hoek.clone(fixture.user)
          const oldContext = Hoek.clone(fixture.context)
          fixture.subject(fixture.user, fixture.context, (err, user, context) => {
            try {
              should(err).not.be.ok()
              user.should.deepEqual(oldUser)
              const raas = context.accessToken[common.raasClaim]
              raas.should.be.an.Array()
              raas.forEach(raa => {
                raa.should.be.a.String()
                console.log(`- ${raa}`)
              })

              const expectedOks = administrator
                ? groupType === 'team'
                  ? expectedAuthorizedAdministratorTeam
                  : expectedAuthorizedAdministratorCompany
                : expectedAuthorized
              expectedOks.forEach(ea => {
                const m = mm.isMatch(ea, raas)
                console.log(m, ea)
                m.should.be.true()
              })
              expectedNotAuthorized.forEach(ea => {
                const m = mm.isMatch(ea, raas)
                console.log(m, ea)
                m.should.be.false()
              })
              oldContext.accessToken[common.raasClaim] = raas
              context.should.deepEqual(oldContext)
              done()
            } catch (e) {
              done(e)
            }
          })
        }

        it('adds raas for the account, with no group administrations', function (done) {
          itWorks(this, done)
        })
        it('runs without debug mode', function (done) {
          // covers a branch we otherwise miss
          global.configuration[common.debugPropertyName] = false
          itWorks(this, done)
        })
        it('runs without a sessionID', function (done) {
          delete this.context.sessionID
          itWorks(this, done)
        })
        const otherParameterLocations = parameterLocations.filter(pl => pl !== parameterLocation)
        otherParameterLocations.forEach(opl => {
          it(`runs without a ${opl}`, function (done) {
            delete this.context.request[opl]
            itWorks(this, done)
          })
        })
        describe('with administrations', function () {
          beforeEach(function () {
            this.dynamodbFixture = new DynamodbFixture()
            const mode = this.context.request[parameterLocation].mode
            return Promise.all(
              teamIds
                .map(id => createGroupAdministrationItem('Team', mode, id))
                .concat(companyIds.map(id => createGroupAdministrationItem('Company', mode, id)))
                .map(item => this.dynamodbFixture.putItem(item))
            )
          })
          afterEach(function () {
            this.dynamodbFixture.clean()
          })

          it('works with some team and company administrations', function (done) {
            itWorks(this, done, true)
          })
        })
      })
    })
  })
})
