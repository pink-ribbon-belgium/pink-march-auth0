/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const path = require('path')

const audience = 'https://app.pink-march.pink-ribbon-belgium.org/'

function createCommon (subjectName) {
  const namespace = 'https://pink-ribbon-belgium.org/'
  const accountIdClaim = `${namespace}accountId`
  const modeClaim = `${namespace}mode`
  const raasClaim = `${namespace}raas`
  return {
    audience,
    scopes: ['openid', 'profile', 'email', 'pink-march-service:access'],
    namespace,
    modeParameterName: 'mode', // no namespace (difficult in query)
    accountIdLength: 10,
    accountIdPattern: /^[a-zA-Z0-9]{10}$/,
    accountIdClaim,
    modeClaim,
    raasClaim,
    subjectName,
    srcPath: path.join(__dirname, '..', 'rules', `${subjectName}.js`),
    debugPropertyName: `${subjectName}-debug`
  }
}

module.exports = createCommon
