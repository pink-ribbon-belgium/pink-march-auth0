/**
 * Emulate Auth0 Global UnauthorizedError
 */
const UnauthorizedError = function (message) {
  const tmp = Error.apply(this, arguments)
  tmp.name = 'UnauthorizedError'
  this.name = tmp.name
  this.message = tmp.message
  Object.defineProperty(this, 'stack', {
    get: function () {
      return tmp.stack
    }
  })
}
UnauthorizedError.prototype = Object.create(Error.prototype)

module.exports = UnauthorizedError
