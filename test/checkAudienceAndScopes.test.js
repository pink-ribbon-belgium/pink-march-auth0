/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('./_testName')(module)
const should = require('should')
const testSourceSyntax = require('./_testSourceSyntax')
const commonFixtureSetup = require('./_commonFixtureSetup')
const setupBasicUserAndContextAndUnauthorizedTest = require('./_setupBasicUserAndContextAndUnauthorizedTest')
const Hoek = require('@hapi/hoek')

const common = require('./_common')('checkAudienceAndScopes')

describe(testName, function () {
  testSourceSyntax(common)
  describe('runs with', function () {
    commonFixtureSetup(module, common)

    const parameterLocations = ['query', 'body']
    parameterLocations.forEach(parameterLocation => {
      describe(parameterLocation, function () {
        setupBasicUserAndContextAndUnauthorizedTest(common, parameterLocation, true)

        function itWorks (fixture, done) {
          const oldUser = Hoek.clone(fixture.user)
          const oldContext = Hoek.clone(fixture.context)
          fixture.subject(fixture.user, fixture.context, (err, user, context) => {
            try {
              should(err).not.be.ok()
              user.should.deepEqual(oldUser)
              context.should.deepEqual(oldContext)
              done()
            } catch (e) {
              done(e)
            }
          })
        }

        it('grants with an appropriate scope and audience', function (done) {
          itWorks(this, done)
        })
        it('runs without debug mode', function (done) {
          // covers a branch we otherwise miss
          global.configuration[common.debugPropertyName] = false
          itWorks(this, done)
        })
        it('grants without a sessionID', function (done) {
          delete this.context.sessionID
          itWorks(this, done)
        })
        const otherParameterLocations = parameterLocations.filter(pl => pl !== parameterLocation)
        otherParameterLocations.forEach(opl => {
          it(`grants without a ${opl}`, function (done) {
            delete this.context.request[opl]
            itWorks(this, done)
          })
        })
        it('grants with less than all the supported scopes', function (done) {
          this.context.request[parameterLocation].scope = 'openid'
          itWorks(this, done)
        })
        it('grants without scopes', function (done) {
          delete this.context.request[parameterLocation].scope
          itWorks(this, done)
        })
        it('grants with empty scopes', function (done) {
          this.context.request[parameterLocation].scope = ''
          itWorks(this, done)
        })
        it('throws UnauthorizedError without a query and without a body', function (done) {
          delete this.context.request.query
          delete this.context.request.body
          this.shouldBeUnauthorized(this.user, this.context, [/audience.*mandatory/, /but not found in/], done)
        })
        it('throws UnauthorizedError without an audience', function (done) {
          delete this.context.request[parameterLocation].audience
          this.shouldBeUnauthorized(this.user, this.context, [/audience.*mandatory/, /but not found in/], done)
        })
        it('throws UnauthorizedError with the wrong audience', function (done) {
          this.context.request[parameterLocation].audience = 'wrong audience'
          this.shouldBeUnauthorized(this.user, this.context, [/audience.*not supported in/], done)
        })
        it('throws UnauthorizedError with unsupported scope', function (done) {
          this.context.request[parameterLocation].scope = 'openid unsupported email'
          this.shouldBeUnauthorized(this.user, this.context, [/unsupported scopes in/], done)
        })
        it('throws UnauthorizedError with scopes with wrong separator', function (done) {
          this.context.request[parameterLocation].scope = 'openid, profile, email'
          this.shouldBeUnauthorized(this.user, this.context, [/unsupported scopes in/], done)
        })
      })
    })
  })
})
