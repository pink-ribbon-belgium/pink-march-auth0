/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const { v4: uuidv4 } = require('uuid')
const AWS = require('aws-sdk')

const automatedTestRoleNameAndPath = 'automated-test/automated-test-pink-march-auth0'
const devsecopsProfile = 'pink-ribbon-belgium-dev'
const region = 'eu-west-1'

AWS.config.apiVersions = {
  dynamodb: '2012-08-10'
}
AWS.config.update({ region })

function getAccountId (AWS) {
  const iam = new AWS.IAM()
  return iam
    .getUser({})
    .promise()
    .then(userData => {
      const accountId = userData.User.Arn.split(':')[4]
      console.info('  AWS user data retrieved') // do not log accountId
      return accountId
    })
}

let testCredentialsPromise

function awsTestCredentials () {
  if (!testCredentialsPromise) {
    if (!process.env.CI) {
      console.info('Not working on CI. Using AWS credentials from `~/.aws/credentials`')
      // get credentials from ~/.aws/credentials for `profile`, get role `automatedTestRoleNameAndPath` with those
      // credentials, and continue with the role credentials
      AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: devsecopsProfile })
      const roleSessionName = `pink-march-auth0-${uuidv4()}`
      testCredentialsPromise = getAccountId(AWS)
        .then(accountId => {
          const sts = new AWS.STS()
          return sts
            .assumeRole({
              RoleArn: `arn:aws:iam::${accountId}:role/${automatedTestRoleNameAndPath}`,
              RoleSessionName: roleSessionName
            })
            .promise()
        })
        .then(stsData => {
          console.info(
            `  AWS credentials for role ${automatedTestRoleNameAndPath} received (session name: ${roleSessionName}). Assuming.`
          )
          return {
            accessKeyId: stsData.Credentials.AccessKeyId,
            secretAccessKey: stsData.Credentials.SecretAccessKey,
            sessionToken: stsData.Credentials.SessionToken
          }
        })
    } else {
      // else, working on a CI system: get the credentials from env variables, and continue with those
      console.info('Working on CI. Using AWS credentials from env $AWS_ACCESS_KEY_ID and $AWS_SECRET_ACCESS_KEY')
      testCredentialsPromise = Promise.resolve({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
      })
    }
  }
  return testCredentialsPromise
}

module.exports = awsTestCredentials
