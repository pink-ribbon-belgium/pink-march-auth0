/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const Hoek = require('@hapi/hoek')
// some object copied from Auth0
const userJson = require('./_util/user')
const contextJson = require('./_util/context')
const requestParametersJson = require('./_util/requestParameters')
const { v4: uuidv4 } = require('uuid')
const UnauthorizedError = require('./_util/UnauthorizedError')
const should = require('should')
const awsTestCredentials = require('./_awsTestCredentials')

function setupBasicUserAndContextAndUnauthorizedTest (common, parameterLocation, checkAudienceAndScope, checkMode) {
  beforeEach(function () {
    this.timeout(10000) // takes some time to get account credentials

    console.log()

    this.user = Hoek.clone(userJson)
    this.context = Hoek.clone(contextJson)

    this.context.sessionID = uuidv4()
    this.context.request[parameterLocation] = Hoek.clone(requestParametersJson)
    this.context.request[parameterLocation].audience = common.audience
    this.context.request[parameterLocation].scope = common.scopes.join(' ')
    this.context.request[parameterLocation].mode = `automated-test-${uuidv4()}`
    this.user.app_metadata = {
      someAppMetaDate: 'someAppMetaDate'
    }

    this.shouldBeUnauthorized = function (userIn, contextIn, regexps, done) {
      const oldUser = Hoek.clone(userIn)
      const oldContext = Hoek.clone(contextIn)
      this.subject(userIn, contextIn, (err, user, context) => {
        try {
          const extendedRegExps = regexps.concat([
            new RegExp(this.context.sessionID),
            new RegExp(this.context.clientName),
            new RegExp(this.context.clientID),
            new RegExp(this.user.user_id),
            new RegExp(this.user.name),
            new RegExp(this.user.nickname),
            new RegExp(this.user.email)
          ])
          if (checkAudienceAndScope) {
            extendedRegExps.push(
              new RegExp(
                `'${this.context.request[parameterLocation] && this.context.request[parameterLocation].audience}'`
              )
            )
            extendedRegExps.push(
              new RegExp(
                `'${this.context.request[parameterLocation] && this.context.request[parameterLocation].scope}'`
              )
            )
          }
          if (checkMode) {
            extendedRegExps.push(
              new RegExp(
                `'${this.context.request[parameterLocation] &&
                  this.context.request[parameterLocation][common.modeParameterName]}'`
              )
            )
          }

          console.log()
          console.log(err)
          err.should.be.Error()
          err.should.be.instanceOf(UnauthorizedError)
          extendedRegExps.forEach(r => {
            err.message.should.match(r)
          })
          should(user).be.undefined()
          this.user.should.deepEqual(oldUser)
          should(context).be.undefined()
          this.context.should.deepEqual(oldContext)
          done()
        } catch (e) {
          done(e)
        }
      })
    }

    return awsTestCredentials().then(credentials => {
      global.configuration.AWSaccessKeyId = credentials.accessKeyId
      global.configuration.AWSsecretAccessKey = credentials.secretAccessKey
      global.configuration.AWSSessionToken = credentials.sessionToken
    })
  })

  afterEach(function () {
    delete this.user
    delete this.context
    delete this.shouldBeUnauthorized
  })
}

module.exports = setupBasicUserAndContextAndUnauthorizedTest
