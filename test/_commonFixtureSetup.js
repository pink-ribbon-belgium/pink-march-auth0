/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const fs = require('fs')
const util = require('util')
const path = require('path')
const UnauthorizedError = require('./_util/UnauthorizedError')
const sinon = require('sinon')

const readFile = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)
const unlink = util.promisify(fs.unlink)

function commonFixtureSetup (testModule, common) {
  const subjectModuleFileName = `${common.subjectName}Module.js`
  const subjectModulePath = path.join(path.dirname(common.srcPath), subjectModuleFileName)
  const subjectModuleReference = path.relative(path.dirname(testModule.filename), subjectModulePath)

  before(function () {
    return readFile(common.srcPath, { encoding: 'utf-8' })
      .then(subjectTxt => {
        // make it a module
        // noinspection SpellCheckingInspection
        const moduleTxt = subjectTxt + `\n\nmodule.exports=${common.subjectName}`
        return writeFile(subjectModulePath, moduleTxt)
      })
      .then(() => {
        this.subject = require(subjectModuleReference)
      })
  })

  after(function () {
    return unlink(subjectModulePath)
  })

  beforeEach(function () {
    // global context for function under test
    global.UnauthorizedError = UnauthorizedError
    global.configuration = {
      [common.debugPropertyName]: true
    }
    global.auth0 = {
      users: {
        updateAppMetadata: sinon.stub().resolves()
      }
    }
  })

  afterEach(function () {
    delete global.UnauthorizedError
    delete global.configuration
    global.auth0.users.updateAppMetadata.reset()
    delete global.auth0
  })
}

module.exports = commonFixtureSetup
